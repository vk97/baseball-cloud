import React, { useEffect, useMemo } from 'react';
import { useAsync } from 'react-async-hook';
import Card from 'src/components/Card';
import EmptyData from 'src/components/EmptyData';
import LoaderData from 'src/components/LoaderData';
import Table from 'src/components/Table';
import FetchData from 'src/utils/FetchData';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import './styles.scss';

interface SessionHittingProps {
  idEvent: string;
  idProfile: string;
}

const SessionHitting = ({
  idEvent,
  idProfile,
}: SessionHittingProps): JSX.Element => {
  const asyncSessionHitting = useAsync(FetchData.fetchEventHittingSummary, [
    idEvent,
    idProfile,
  ]);

  const dataTable = useMemo(() => {
    return (
      asyncSessionHitting.result?.data.data.event_hitting_summary
        .hitting_summary_rows || []
    );
  }, [asyncSessionHitting.result]);
  const columnTable = useMemo(
    () => [
      { Header: 'Batter Name', accessor: 'batter_name' },
      { Header: 'Pitcher Name', accessor: 'pitcher_name' },
      { Header: 'Pitch Type', accessor: 'pitch_type' },
      { Header: 'Exit Velocity', accessor: 'exit_velocity' },
      { Header: 'Launch Angle', accessor: 'launch_angle' },
      { Header: 'Distance', accessor: 'distance' },
      { Header: 'Pitch Call', accessor: 'pitch_call' },
    ],
    [],
  );

  useEffect(() => {
    return () => {
      asyncSessionHitting.reset();
    };
  }, []);

  const dataCharts = dataTable.map((hitting) => [
    hitting.batter_name,
    hitting.exit_velocity,
  ]);

  const options: Highcharts.Options = {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Exit Velocity',
    },
    xAxis: {
      type: 'linear',
      labels: {
        rotation: -45,
        format: '{point.y:.1f}',
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Velocity',
      },
    },
    legend: {
      enabled: true,
    },
    tooltip: {
      pointFormat: 'Exit Velocity: <b>{point.y:.1f}</b>',
    },
    series: [
      {
        type: 'column',
        name: 'Velocity',
        data: dataCharts,
      },
    ],
  };

  return (
    <Card
      title="Data collected for all hitters who participated in this session."
      classElement="session-hitting">
      <div className="session-hitting__chart"></div>
      <div className="session-hitting__table">
        {asyncSessionHitting.loading && <LoaderData />}
        {asyncSessionHitting.result && dataTable.length > 0 && (
          <>
            <HighchartsReact highcharts={Highcharts} options={options} />
            <Table columns={columnTable} data={dataTable} />
          </>
        )}

        {dataTable.length === 0 && !asyncSessionHitting.loading && (
          <EmptyData />
        )}
      </div>
    </Card>
  );
};

export default SessionHitting;
