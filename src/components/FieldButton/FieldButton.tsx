import React from 'react';
import { ButtonType } from 'src/types';
import classNames from 'classnames';
import './styles.scss';

interface FieldButtonProps {
  title: string;
  buttonType?: ButtonType;
  onClick?: () => void;
  disabled?: boolean;
}

const FieldButton = ({
  title,
  buttonType,
  onClick,
  disabled,
}: FieldButtonProps): JSX.Element => {
  const classButton = classNames('button', {
    'button--submit': buttonType === ButtonType.SUBMIT,
    'button--transparent': buttonType === ButtonType.TRANSPARENT,
    'button--danger': buttonType === ButtonType.DANGER,
  });

  return (
    <button
      className={`button ${classButton}`}
      type={buttonType === ButtonType.SUBMIT ? 'submit' : 'button'}
      onClick={onClick}
      disabled={disabled}>
      {title}
    </button>
  );
};

FieldButton.defaultProps = {
  buttonType: ButtonType.SUBMIT,
  onClick: () => undefined,
  disabled: false,
} as Partial<FieldButtonProps>;

export default FieldButton;
