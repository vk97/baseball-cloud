import { createAsyncThunk } from '@reduxjs/toolkit';
import { customHistory } from 'src/App';
import { QUERY_CURRENT_PROFILE, QUERY_UPDATE_PROFILE } from 'src/const';
import { FetchApi, UserSignInRequest, UserSignUpRequest } from 'src/types';
import { RootState } from '../reducer';
import { AppDispatch } from '../store';
import {
  ErrorSignUp,
  ErrorUpdate,
  RequestCurrentProfile,
  RequestForgotPassword,
  RequestUserData,
  ResponseUserData,
  UpdateProfileResponse,
  UserProfile,
} from './types';
import userActionTypes from './types';
import { fetchApi } from 'src/utils/fetchApi';

const fetchCurrentProfile = createAsyncThunk<
  RequestCurrentProfile,
  unknown,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: FetchApi<RequestCurrentProfile>;
  }
>(userActionTypes.GET_CURRENT_PROFILE, async (userProfile, thunkApi) => {
  const data = {
    query: QUERY_CURRENT_PROFILE,
  };

  const response = await thunkApi.extra('/api/v1/graphql', data);

  return response.data as RequestCurrentProfile;
});

const fetchSignIn = createAsyncThunk<ResponseUserData, UserSignInRequest>(
  userActionTypes.SIGN_IN_USER,
  async (userData) => {
    const response = await fetchApi<RequestUserData>(
      '/api/v1/auth/sign_in',
      userData,
    );

    customHistory.push('/profile');

    return {
      user: response.data.data,
      accessToken: response.headers['access-token'],
      client: response.headers['client'],
    };
  },
);

const fetchSignUp = createAsyncThunk<
ResponseUserData,
  UserSignUpRequest,
  {
    rejectValue: ErrorSignUp;
  }
>(userActionTypes.SIGN_UP_USER, async (userData, thunkApi) => {
  const response = await fetchApi<RequestUserData>(
    '/api/v1/auth',
    userData,
  );

  customHistory.push('/profile');

  return {
    user: response.data.data,
    accessToken: response.headers['access-token'],
    client: response.headers['client'],
  };
});

const fetchForgotPassword = createAsyncThunk<
  RequestUserData,
  RequestForgotPassword,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: FetchApi<RequestUserData>;
  }
>(userActionTypes.FORGOT_PASSWORD, async (userData, { extra }) => {
  const response = await extra('/resetpassword', userData);

  return response.data;
});

const fetchUpdateProfile = createAsyncThunk<
  UpdateProfileResponse,
  UserProfile,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: any;
    rejectValue: ErrorUpdate;
  }
>(userActionTypes.UPDATE_PROFILE, async (userProfile, thunkApi) => {
  const data = {
    query: QUERY_UPDATE_PROFILE,
    variables: {
      form: userProfile,
    },
  };

  const response = await thunkApi.extra('/api/v1/graphql', data);

  if (response.status === 400) {
    return thunkApi.rejectWithValue((await response.data) as ErrorUpdate);
  }

  return response.data as UpdateProfileResponse;
});

export default {
  fetchSignIn,
  fetchSignUp,
  fetchForgotPassword,
  fetchCurrentProfile,
  fetchUpdateProfile,
};
