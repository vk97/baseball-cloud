import { Facilities, Schools, Teams } from 'src/types';

const USER_LOGOUT = 'user/logout';
const SIGN_IN_USER = 'user/signInUser';
const SIGN_UP_USER = 'user/signUpUser';
const FORGOT_PASSWORD = 'user/forgotPassword';
const GET_CURRENT_PROFILE = 'user/getCurrentProfile';
const UPDATE_PROFILE = 'user/updateProfile';

export interface UserData {
  direct_paid?: boolean;
  email: string;
  id: number;
  paid?: boolean;
  plan_id?: number;
  role: string | null;
  team_avatar?: any;
  team_user?: boolean;
  u_name?: number;
  uid?: string;
  unsubscribe?: boolean;
}

export interface UserProfile {
  age: number;
  avatar: null | string;
  bats_hand: null | string;
  biography: null | string;
  facilities: Facilities;
  feet: number;
  first_name: null | string;
  id: string;
  inches: number;
  last_name: null | string;
  position: null | string;
  position2: null | string;
  school: Schools;
  school_year: null | string;
  teams: Teams;
  throws_hand: null | string;
  weight: number;
  recent_events?: [];
}

export interface RequestUserData {
  data: UserData;
}

export interface ResponseUserData {
  user: UserData;
  accessToken: string;
  client: string;
}

export interface RequestForgotPassword {
  email: string;
}

export interface RequestCurrentProfile {
  data: {
    current_profile: UserProfile;
  };
}

export interface ResponseProfileData {
  data: { profile: UserProfile };
}

export interface ResponsePlayerProfileError {
  message: string;
}

export type RequestProfileResponse =
  | ResponseProfileData
  | ResponsePlayerProfileError;

export interface UpdateProfileResponse {
  data: {
    update_profile: {
      profile: UserProfile;
    };
  };
}

export interface ErrorUpdate {
  errorMessage: string;
}

export interface ErrorSingIn {
  success: boolean;
  errors: string[];
}

export interface ErrorSignUp {
  errors: {
    email: string[];
    full_messages: string[];
  };
}

export default {
  USER_LOGOUT,
  SIGN_IN_USER,
  SIGN_UP_USER,
  FORGOT_PASSWORD,
  GET_CURRENT_PROFILE,
  UPDATE_PROFILE,
};
