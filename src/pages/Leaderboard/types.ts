import { School, Teams } from "src/types";

export interface LeaderBoardBattingType {
  age: number;
  batter_datraks_id: number;
  batter_name: string;
  distance: number;
  exit_velocity: number;
  favorite: boolean;
  launch_angle: number;
}

export interface LeaderBoardPitchingType {
  age: number;
  favorite: boolean;
  horizontal_break: number;
  pitch_type: string;
  pitcher_datraks_id: number;
  pitcher_name: string;
  school: School;
  spin_rate: number;
  teams: Teams;
  velocity: number;
  vertical_break: number;
}

export interface LeaderBoardErrorResponse {
  data: {
    message: string;
  };
}

export interface LeaderBoardBattingResponse {
  data: {
    leaderboard_batting: {
      leaderboard_batting: Array<LeaderBoardBattingType>;
    };
  };
}

export interface LeaderBoardPitchingResponse {
  data: {
    leaderboard_pitching: {
      leaderboard_pitching: Array<LeaderBoardPitchingType>;
    };
  };
}

export type LeaderBoardType =
  | Array<LeaderBoardBattingType>
  | Array<LeaderBoardPitchingType> | [];
