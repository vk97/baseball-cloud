import React from 'react';
import MainContainer from '../../components/MainContainer';
import FormAuth from '../../components/FormAuth';
import { MainLayoutModification } from 'src/types';

const SignIn = (): JSX.Element => {
  return (
    <MainContainer styleModification={MainLayoutModification.AUTHORIZATION}>
      <FormAuth />
    </MainContainer>
  );
};

export default SignIn;
