import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import baseAvatarUser from 'src/assets/images/user-avatar.png';
import iconAge from 'src/assets/images/icon-age.svg';
import iconHeight from 'src/assets/images/icon-height.svg';
import iconWeight from 'src/assets/images/icon-weight.svg';
import iconThrows from 'src/assets/images/icon-throws.svg';
import iconBats from 'src/assets/images/icon-bats.svg';
import iconEdit from 'src/assets/images/icon-button-edit.svg';
import { userSelectors } from 'src/redux/user';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faHeart as faHeartRegular } from '@fortawesome/free-regular-svg-icons';
import { getLabelPosition } from 'src/utils/utils';
import './styles.scss';
import { PlayerProfile } from 'src/redux/data/types';
import { useAsyncCallback } from 'react-async-hook';
import FetchData from 'src/utils/FetchData';
import { changeStatusFavorite } from 'src/redux/playerProfile/playerProfileSlice';
import { AppDispatch } from 'src/redux/store';
import { toast } from 'react-toastify';

interface ProfileInfoProps {
  profile: PlayerProfile;
  onChangeStatusEdit?: () => void;
}

const ProfileInfo = ({
  profile,
  onChangeStatusEdit,
}: ProfileInfoProps): JSX.Element => {
  const dispatch: AppDispatch = useDispatch();
  const currentProfile = useSelector(userSelectors.selectCurrentProfile);

  const handleUpdateFavorite = useAsyncCallback(
    () => {
      return FetchData.fetchUpdateFavorite(profile.id, !profile.favorite);
    },
    {
      onSuccess(result) {
        if (result.status === 200 && result.data.data.update_favorite_profile) {
          dispatch(changeStatusFavorite());
          toast.success(
            `This profile ${
              profile.favorite ? 'removed' : 'added'
            } to favorite list successfully.`,
          );
        }
      },
    },
  );

  return (
    <div className="user-profile__data profile-data">
      {currentProfile && currentProfile.id === profile.id ? (
        <button
          onClick={onChangeStatusEdit}
          type="button"
          className="button profile-data__button-edit"
          aria-label="Edit">
          <img src={iconEdit} alt="Icon Edit" />
        </button>
      ) : (
        <button
          onClick={() => handleUpdateFavorite.execute()}
          type="button"
          className="button profile-data__button-favorite"
          aria-label="Edit">
          {profile.favorite ? (
            <FontAwesomeIcon icon={faHeart} color="#48bbff" />
          ) : (
            <FontAwesomeIcon icon={faHeartRegular} color="#48bbff" />
          )}
        </button>
      )}

      <div className="profile-data__container-image">
        <img src={baseAvatarUser} alt="User Avatar" />
      </div>
      <span className="profile-data__fio">
        {profile.first_name} {profile.last_name}
      </span>
      <span className="profile-data__position-game">
        {getLabelPosition(profile.position)}
      </span>
      <span className="profile-data__position-game profile-data__position-game--secondary">
        {getLabelPosition(profile.position2)}
      </span>

      <table className="profile-data__table personal-info">
        <thead>
          <tr className="personal-info__row">
            <td className="personal-info__coll personal-info__coll--icon">
              <img
                width="22"
                height="21"
                className="personal-info__icon"
                src={iconAge}
                alt="Icon age"
              />
            </td>
            <td className="personal-info__coll personal-info__coll--name-value">
              Age
            </td>
            <td className="personal-info__coll personal-info__coll--value">
              {profile.age}
            </td>
          </tr>
          <tr className="personal-info__row">
            <td className="personal-info__coll personal-info__coll--icon">
              <img
                width="12"
                height="23"
                className="personal-info__icon"
                src={iconHeight}
                alt="Icon height"
              />
            </td>
            <td className="personal-info__coll personal-info__coll--name-value">
              Height
            </td>
            <td className="personal-info__coll personal-info__coll--value">
              {profile.feet} ft 0 in
            </td>
          </tr>
          <tr className="personal-info__row">
            <td className="personal-info__coll personal-info__coll--icon">
              <img
                width="14"
                height="17"
                className="personal-info__icon"
                src={iconWeight}
                alt="Icon weight"
              />
            </td>
            <td className="personal-info__coll personal-info__coll--name-value">
              Weight
            </td>
            <td className="personal-info__coll personal-info__coll--value">
              {profile.weight}
            </td>
          </tr>
          <tr className="personal-info__row">
            <td className="personal-info__coll personal-info__coll--icon">
              <img
                width="18"
                height="18"
                className="personal-info__icon"
                src={iconThrows}
                alt="Icon throws"
              />
            </td>
            <td className="personal-info__coll personal-info__coll--name-value">
              Throws
            </td>
            <td className="personal-info__coll personal-info__coll--value">
              {profile.throws_hand?.toLocaleUpperCase()}
            </td>
          </tr>
          <tr className="personal-info__row">
            <td className="personal-info__coll personal-info__coll--icon">
              <img
                width="20"
                height="20"
                className="personal-info__icon"
                src={iconBats}
                alt="Icon bats"
              />
            </td>
            <td className="personal-info__coll personal-info__coll--name-value">
              Bats
            </td>
            <td className="personal-info__coll personal-info__coll--value">
              {profile.bats_hand?.toLocaleUpperCase()}
            </td>
          </tr>
        </thead>
      </table>
      <section className="school-info">
        <h2 className="visually-hidden">Shool Info</h2>
        {profile.school && (
          <p className="profile-data__block school-info__group">
            <span className="school-info__header">School</span>
            <span className="school-info__text">
              {profile.school && profile.school.name}
            </span>
          </p>
        )}
        {profile.school_year && (
          <p className="profile-data__block school-info__group">
            <span className="school-info__header">School Year</span>
            <span className="school-info__text">{profile.school_year}</span>
          </p>
        )}
        {profile.teams.length > 0 && (
          <p className="profile-data__block school-info__group">
            <span className="school-info__header">Team</span>
            <span className="school-info__text">
              {(profile.teams as any[]).map((team) => team.name).join(',')}
            </span>
          </p>
        )}
      </section>

      {profile.biography && (
        <div className="profile-data__block about-info">
          <div className="about-info__header">
            <h2 className="about-info__title">About</h2>
          </div>
          <p className="about-info__text">{profile.biography}</p>
        </div>
      )}
    </div>
  );
};

ProfileInfo.defaultProps = {
  onChangeStatusEdit: () => undefined,
} as Partial<ProfileInfoProps>;

export default ProfileInfo;
