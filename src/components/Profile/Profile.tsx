import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import userBaseAvatar from 'src/assets/images/user-avatar.png';
import { userLogout } from 'src/redux/user/actions';
import { UserData } from 'src/redux/user/types';
import { TypeAccount } from 'src/types';
import './styles.scss';

interface ProfileProps {
  userData: UserData;
}

const Profile = ({ userData }: ProfileProps): JSX.Element => {
  const [isOpen, setOpen] = useState(false);
  const dispatch = useDispatch();

  const classDisplayDropDownMenu = isOpen ? 'display-show' : 'display-none';
  return (
    <section className="profile">
      <div className="profile__container-image">
        <Link to="/profile">
          <img src={userBaseAvatar} alt="Profile image" />
        </Link>
      </div>
      <button
        type="button"
        className="profile__button-menu"
        onClick={() => setOpen(!isOpen)}>
        {userData && userData.email}
      </button>
      <ul
        className={`profile__dropdown-menu profile-dropdown-menu ${classDisplayDropDownMenu}`}>
        <li className="profile-dropdown-menu__item">
          {userData.role === TypeAccount.PLAYER ? (
            <Link to="/profile" className="profile-dropdown-menu__link">
              My Profile
            </Link>
          ) : (
            <Link to="/dashboard" className="profile-dropdown-menu__link">
              Dashboard
            </Link>
          )}
        </li>
        <li className="profile-dropdown-menu__item">
          <Link
            to="/login"
            className="profile-dropdown-menu__link"
            onClick={() => dispatch(userLogout())}>
            Log Out
          </Link>
        </li>
      </ul>
    </section>
  );
};

export default Profile;
