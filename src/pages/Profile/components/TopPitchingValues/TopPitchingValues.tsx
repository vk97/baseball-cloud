import React from 'react';
import { ProgressBar } from 'react-bootstrap';
import Card from 'src/components/Card';
import { PitchingSummary, PitchingValue } from 'src/types';

interface TopPitchingValuesProps {
  pitchingValues: PitchingSummary | undefined;
}

const TopPitchingValues = ({
  pitchingValues = {
    average_values: [],
    top_values: [],
  },
}: TopPitchingValuesProps): JSX.Element => {
  const topFastballVelocity =
    pitchingValues.top_values.length > 0
      ? (pitchingValues.top_values as PitchingValue[]).reduce(
          (acc: PitchingValue, curr: PitchingValue) =>
            acc.velocity > curr.velocity ? acc : curr,
        ).velocity
      : 0;
  const topSpinRate =
    pitchingValues.top_values.length > 0
      ? (pitchingValues.top_values as PitchingValue[]).reduce(
          (acc: PitchingValue, curr: PitchingValue) =>
            acc.spin_rate > curr.spin_rate ? acc : curr,
        ).spin_rate
      : 0;

  return (
    <Card classElement="pitcher-summary" title="Top Pitching Values">
      <ul className="pitcher-summary__list pitcher-summary-list">
        <li className="pitcher-summary-list__item">
          <span className="pitcher-summary-list__item-title">
            Fastball Velocity
          </span>
          <span className="pitcher-summary-list__item-value">
            {topFastballVelocity || 'N/A'}
          </span>
          <ProgressBar variant="warning" now={topFastballVelocity} />
        </li>
        <li className="pitcher-summary-list__item">
          <span className="pitcher-summary-list__item-title">Spin Rate</span>
          <span className="pitcher-summary-list__item-value">
            {topSpinRate || 'N/A'}
          </span>
          <ProgressBar variant="warning" now={topSpinRate} />
        </li>
        <li className="pitcher-summary-list__item">
          <span className="pitcher-summary-list__item-title">
            Pitch Movement
          </span>
          <span className="pitcher-summary-list__item-value">
            {false || 'N/A'}
          </span>
          <ProgressBar variant="warning" now={0} />
        </li>
      </ul>
    </Card>
  );
};

export default TopPitchingValues;
