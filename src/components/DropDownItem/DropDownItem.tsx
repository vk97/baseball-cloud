import React from 'react';
import './styles.scss';

type DropDownItemProps = {
  itemLists: string[];
  onClick: (value: string) => void;
};

const DropDownItem = ({
  itemLists,
  onClick,
}: DropDownItemProps): JSX.Element => {
  const listItems = itemLists.map((item, index) => {
    const isItemReset =
      item.toLocaleUpperCase() === 'ALL' || item.toLocaleUpperCase() === 'NONE';

    return (
      <li className="dropdown-list__item" key={item + index}>
        <a
          className="dropdown-list__link"
          onClick={isItemReset ? () => onClick('') : () => onClick(item)}>
          {item}
        </a>
      </li>
    );
  });

  return <ul className="dropdown-list">{listItems}</ul>;
};

export default DropDownItem;
