import React, { useState } from 'react';
import SideBarLeft from 'src/components/SideBarLeft';
import { PlayerProfile } from 'src/redux/data/types';
import {
  checkCompletedProfileData,
  checkPositionPitcher,
} from 'src/utils/utils';
import FormEditProfile from '../FormEditProfile';
import ProfileInfo from '../ProfileInfo';
import ProfileNotCompleted from '../ProfileNotCompleted';
import UserProfileReport from '../UserProfileReport';

interface CurrentProfileProps {
  userProfile: PlayerProfile;
}

const CurrentProfile = ({ userProfile }: CurrentProfileProps): JSX.Element => {
  const [isEdit, setEdit] = useState(false);
  const isNoEmptyData = checkCompletedProfileData(userProfile);

  return (
    <>
      {!isNoEmptyData || isEdit ? (
        <SideBarLeft>
          <FormEditProfile
            profile={userProfile}
            onChangeStatusEdit={() => setEdit(false)}
          />
        </SideBarLeft>
      ) : (
        <SideBarLeft>
          <ProfileInfo
            profile={userProfile}
            onChangeStatusEdit={() => setEdit(true)}
          />
        </SideBarLeft>
      )}
      {isNoEmptyData ? (
        <UserProfileReport
          userProfile={userProfile}
          isPitcher={checkPositionPitcher(userProfile)}
        />
      ) : (
        <ProfileNotCompleted />
      )}
    </>
  );
};

export default CurrentProfile;
