import React, { useState } from 'react';
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs';
import Card from 'src/components/Card';
import Comparison from 'src/components/Comparison';
import RecentSessionReports from 'src/components/RecentSessionReports';
import TopBattingValues from '../TopBattingValues';
import TopPitchingValues from '../TopPitchingValues';
import { userSelectors } from 'src/redux/user';
import { useSelector } from 'react-redux';
import { useAsync } from 'react-async-hook';
import { PlayerProfile } from 'src/redux/data/types';
import { UserProfile } from 'src/redux/user/types';
import FetchData from 'src/utils/FetchData';
import DropDownItem from 'src/components/DropDownItem';
import SessionReports from '../SessionReports';
import BattingReportPanel from '../BattingReportPanel';
import { VariantBattingAndPitchingType } from './types';
import PitchingReportPanel from '../PitchingReportPanel';

interface UserProfileReportProps {
  userProfile: PlayerProfile;
  isPitcher: boolean;
  isCurrentProfile?: boolean;
}

const UserProfileReport = ({
  userProfile,
  isPitcher,
  isCurrentProfile,
}: UserProfileReportProps): JSX.Element => {
  const [tabIndex, setTabIndex] = useState(0);
  const userData = useSelector(userSelectors.selectUserData);
  const [variant, setVariant] = useState<VariantBattingAndPitchingType>(
    'Summary',
  );

  const battingSummaryAsync = useAsync(FetchData.fetchBattingSummary, [
    userProfile.id,
  ]);
  const pitchingSummaryAsync = useAsync(FetchData.fetchPitchingSummary, [
    userProfile.id,
  ]);
  const profileEventsAsync = useAsync(FetchData.fetchEvents, [userProfile.id]);

  return (
    <div className="user-profile__report">
      <TopBattingValues
        battingValues={battingSummaryAsync.result?.data.data.batting_summary}
      />
      {isPitcher && (
        <TopPitchingValues
          pitchingValues={
            pitchingSummaryAsync.result?.data.data.pitching_summary
          }
        />
      )}
      {(isCurrentProfile || userData?.role === 'scout') && (
        <RecentSessionReports
          profileList={[
            {
              id: userProfile.id,
              recent_events:
                profileEventsAsync.result?.data.data.profile_events.events ||
                [],
            },
          ]}
          isLoading={profileEventsAsync.loading}
        />
      )}
      <Card
        title="Profile"
        titleHide={true}
        classElement="user-profile__report-card">
        <Tabs selectedIndex={tabIndex} onSelect={(index) => setTabIndex(index)}>
          <TabList>
            {isPitcher && (
              <Tab>
                Pitching
                <DropDownItem
                  itemLists={['Summary', 'Charts', 'Log']}
                  onClick={(value: any) => {
                    setTabIndex(0);
                    setVariant(value);
                  }}
                />
              </Tab>
            )}
            <Tab>
              Batting
              <DropDownItem
                itemLists={['Summary', 'Charts', 'Log']}
                onClick={(value: any) => {
                  const tabIndex = isPitcher ? 1 : 0;
                  setTabIndex(tabIndex);
                  setVariant(value);
                }}
              />
            </Tab>
            {(isCurrentProfile || userData?.role === 'scout') && (
              <Tab
                onClick={() => {
                  const tabIndex = isCurrentProfile ? 3 : 2;
                  setTabIndex(tabIndex);
                }}>
                Session Reports
              </Tab>
            )}
            <Tab>Сomparison</Tab>
          </TabList>
          {isPitcher && (
            <TabPanel>
              {pitchingSummaryAsync.result && (
                <PitchingReportPanel
                  userProfile={userProfile}
                  pitchingSummary={
                    pitchingSummaryAsync.result?.data.data.pitching_summary
                  }
                  variant={variant}
                />
              )}
            </TabPanel>
          )}
          <TabPanel>
            {battingSummaryAsync.result && (
              <BattingReportPanel
                userProfile={userProfile}
                battingSummary={
                  battingSummaryAsync.result?.data.data.batting_summary
                }
                variant={variant}
              />
            )}
          </TabPanel>
          {(isCurrentProfile || userData?.role === 'scout') && (
            <TabPanel>
              <SessionReports
                id={userProfile.id}
                events={
                  profileEventsAsync.result?.data.data.profile_events.events ||
                  []
                }
              />
            </TabPanel>
          )}
          <TabPanel>
            <Comparison firstProfileId={userProfile.id} />
          </TabPanel>
        </Tabs>
      </Card>
    </div>
  );
};

UserProfileReport.defaultProps = {
  isCurrentProfile: true,
} as Partial<UserProfileReportProps>;

export default UserProfileReport;
