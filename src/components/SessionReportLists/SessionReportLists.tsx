import React from 'react';
import { Link } from 'react-router-dom';
import SwiperCore, { Navigation, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { RecentEventType } from 'src/types';
import IconLock from '../Icons/IconLock';
import baseAvatar from 'src/assets/images/user-avatar.png';
import moment from 'moment';
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import './styles.scss';
import { customHistory } from 'src/App';
import { RecentSessionReportsProfile } from '../RecentSessionReports/RecentSessionReports';
import EmptyData from '../EmptyData';

interface SessionReportListsProps {
  profilesList: RecentSessionReportsProfile[];
  isLoading: boolean;
}

SwiperCore.use([Navigation, A11y]);

const SessionReportLists = ({
  profilesList,
  isLoading,
}: SessionReportListsProps): JSX.Element => {
  const profilesEvents = (profilesList as RecentSessionReportsProfile[])
    .map((profile) => {
      return profile.recent_events;
    })
    .flat();
  <p className="recent-events__text">
    No data currently linked to this profile
  </p>;
  return (
    <>
      <Swiper
        tag="ul"
        containerModifierClass="recent-events-lists "
        spaceBetween={16}
        navigation>
        {(profilesList as RecentSessionReportsProfile[]).map((profile) => {
          return (profile.recent_events as RecentEventType[]).map((report) => {
            return (
              <SwiperSlide
                onClick={() => {
                  customHistory.push(
                    `/profile/${profile.id}/event/${report.id}`,
                  );
                }}
                className="recent-events-lists__item report"
                tag="li"
                key={report.id}>
                <div className="report__type">
                  <span>{report.event_type}</span>
                </div>
                {report.event_type === 'Game' && (
                  <div className="report__lock">
                    <IconLock />
                  </div>
                )}
                <div className="report__info">
                  <div className="report__date">
                    <span className="report__day">
                      {moment(report.date, 'DD-MM-YYYY').date()}
                    </span>
                    <span className="report__month">
                      {moment.monthsShort(
                        moment(report.date, 'DD-MM-YYYY').month(),
                      )}
                    </span>
                  </div>
                  {report.data_rows_count && (
                    <p className="report__text">
                      {report.data_rows_count} Pitches Seen
                    </p>
                  )}
                  <p className="report__text">Session for {report.date}</p>
                </div>
                <ul className="events-user-lists">
                  {report.recent_avatars &&
                    report.recent_avatars.map((user) => {
                      return (
                        <li className="events-user-lists__item" key={user.id}>
                          <Link
                            onClick={(e) => e.stopPropagation()}
                            className="events-user-lists__link"
                            to={`/profile/${user.id}`}>
                            <img
                              width="22"
                              height="22"
                              src={user.avatar || baseAvatar}
                              alt="User Avatar"
                            />
                          </Link>
                        </li>
                      );
                    })}
                </ul>
              </SwiperSlide>
            );
          });
        })}
      </Swiper>
      {profilesEvents.length === 0 && !isLoading && <EmptyData />}
    </>
  );
};

export default SessionReportLists;
