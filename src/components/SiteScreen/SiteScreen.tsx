import React from 'react';
import './styles.scss';

interface SiteScreenProps {
  children: React.ReactNode;
}

const SiteScreen = ({ children }: SiteScreenProps): JSX.Element => {
  return <div className="site-screen">{children}</div>;
};

export default SiteScreen;
