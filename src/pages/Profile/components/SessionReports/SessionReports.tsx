import React, { useEffect, useMemo, useState } from 'react';
import { Field, Form } from 'react-final-form';
import EmptyData from 'src/components/EmptyData';
import IconArrowDown from 'src/components/Icons/IconArrowDown';
import Table from 'src/components/Table';
import DatePicker from 'react-datepicker';
import { RecentEventType } from 'src/types';
import 'react-datepicker/dist/react-datepicker.css';
import Select from 'react-select';
import { customStylesSelect } from 'src/styles/customStyles';
import './styles.scss';
import moment from 'moment';
import { getFilterValue } from 'src/utils/utils';
import FetchData from 'src/utils/FetchData';
import { useAsyncCallback } from 'react-async-hook';
import useDebounce from 'src/hooks/useDebounce';
import LoaderData from 'src/components/LoaderData';

interface SessionReportsProps {
  id: string;
  events: RecentEventType[] | [];
}

interface Filters {
  offset: number;
  date?: string;
  event_type?: string;
}

const SessionReports = ({ id, events }: SessionReportsProps): JSX.Element => {
  const [userEvents, setUserEvents] = useState<RecentEventType[] | []>(events);
  const [errorLoading, setErrorLoading] = useState<string | null>(null);
  const [filterEvents, setFilterEvents] = useState<Filters>({ offset: 0 });
  const columnsSession = useMemo(
    () => [
      { Header: 'Date', accessor: 'date' },
      { Header: 'Type', accessor: 'event_type' },
      { Header: 'Name', accessor: 'event_name' },
      { Header: 'Purchased', accessor: 'purchased' },
    ],
    [],
  );
  const data = useMemo(() => userEvents, [userEvents]);
  const typeOptions = [
    { value: undefined, label: 'None' },
    { value: 'Game', label: 'Game' },
    { value: 'Practice', label: 'Practice' },
  ];

  const handleGetEvents = useAsyncCallback(
    (filterEvents) => {
      setUserEvents([]);
      return FetchData.fetchEvents(id, filterEvents);
    },
    {
      onSuccess: (result) => {
        if (result.data.data) {
          setUserEvents(result.data.data.profile_events.events);
        }
      },
      onError: (err) => {
        setErrorLoading(err.message);
      },
    },
  );

  const debouncedSearchPlayer = useDebounce(filterEvents, 500);

  useEffect(() => {
    if (debouncedSearchPlayer) {
      handleGetEvents.execute(filterEvents);
    }

    return () => {
      setUserEvents([]);
    };
  }, [debouncedSearchPlayer]);

  return (
    <section className="session-reports">
      <h2 className="session-reports__title">Sessions</h2>
      <div className="session-reports__filters session-filters">
        <Form
          initialValues={filterEvents}
          onSubmit={(filters: Filters) => setFilterEvents(filters)}
          render={({ handleSubmit, form, values }) => (
            <form className="session-filters__form session-filters-form">
              <div className="session-filters-form__group">
                <button
                  className="session-filters-form__button"
                  type="button"
                  onClick={() => setFilterEvents({ offset: 0 })}>
                  Clear Filters
                </button>
              </div>
              <Field
                name="date"
                parse={(date: string) => moment(date).format('DD-MM-YYYY')}
                render={({ input }) => {
                  const date = values.date
                    ? new Date(moment(values.date, 'DD-MM-YYYY').format())
                    : new Date();
                  return (
                    <div className="session-filters-form__group">
                      <label
                        className="session-filters-form__label"
                        htmlFor="session-date">
                        {values.date ? `Date (${values.date}) ` : 'Date '}
                        <IconArrowDown
                          width={16}
                          height={9}
                          color={'#48bbff'}
                        />
                      </label>

                      <DatePicker
                        calendarClassName="session-filters-form__calendar"
                        selected={date}
                        onChange={(value: Date) => {
                          input.onChange(value);
                          handleSubmit();
                        }}
                        id="session-date"
                        className="visually-hidden"
                      />
                    </div>
                  );
                }}
              />
              <Field
                name="event_type"
                type="text"
                parse={getFilterValue}
                render={({ input }) => (
                  <div className="session-filters__group">
                    <Select
                      styles={customStylesSelect}
                      options={typeOptions}
                      placeholder="Position"
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                      isSearchable={false}
                    />
                  </div>
                )}
              />
            </form>
          )}
        />
      </div>
      <div className="session-reports__table">
        <Table columns={columnsSession} data={data} />
        {handleGetEvents.loading && <LoaderData />}
      </div>
      {errorLoading !== null && (
        <p className="session-reports__error-loading">{errorLoading}</p>
      )}
      {userEvents.length === 0 && <EmptyData />}
    </section>
  );
};

export default SessionReports;
