import { CSSProperties } from 'react';

interface StateSelect {
  [key: string]: any;
}

export const customStylesSelect = {
  menu: (provided: CSSProperties, state: StateSelect): CSSProperties => ({
    ...provided,
    width: state.selectProps.width,
    minWidth: '100px',
    zIndex: 5,
  }),
  dropdownIndicator: (): CSSProperties => ({
    color: '#48bbff',
  }),
  indicatorSeparator: (): CSSProperties => ({
    display: 'none',
  }),
  placeholder: (): CSSProperties => ({
    color: '#48bbff',
  }),
  singleValue: (): CSSProperties => ({
    color: '#48bbff',
    border: 'none',
  }),
  option: (provided: CSSProperties, state: StateSelect): CSSProperties => ({
    ...provided,
    border: 'none',
    color: '#788b99',
    backgroundColor:
      state.isHover || state.isSelected ? 'rgba(72, 187, 255, 0.1)' : 'white',
  }),
  control: (): CSSProperties => ({
    display: 'flex',
    color: '#48bbff',
    border: 'none',
  }),
};

export const customStylesInputSelect = {
  menu: (provided: CSSProperties, state: StateSelect): CSSProperties => ({
    ...provided,
    width: state.selectProps.width,
    minWidth: '100px',
    zIndex: 5,
  }),
  container: (provided: CSSProperties): CSSProperties => ({
    ...provided,
    minHeight: 'auto !important',
  }),
  dropdownIndicator: (): CSSProperties => ({
    display: 'none',
  }),
  indicatorSeparator: (): CSSProperties => ({
    display: 'none',
  }),
  placeholder: (): CSSProperties => ({
    color: '#48bbff',
  }),
  singleValue: (): CSSProperties => ({
    color: '#788b99',
    border: 'none',
  }),
  option: (provided: CSSProperties, state: StateSelect): CSSProperties => ({
    ...provided,
    border: 'none',
    color: '#788b99',
    backgroundColor:
      state.isHover || state.isSelected ? 'rgba(72, 187, 255, 0.1)' : 'white',
  }),
  control: (state: StateSelect): CSSProperties => ({
    display: 'flex',
    background: 'none',
    backgroundColor: 'none',
    border: 'none',
    outline: 'none',
    color: '#788b99',
    borderBottom: (state.isFocused || state.isHover) && '1px solid #48bbff',
  }),
  input: (): CSSProperties => ({
    outline: 'none',
    minHeight: 'auto',
  }),
};
