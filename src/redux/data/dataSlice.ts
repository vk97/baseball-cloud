import { createSlice } from '@reduxjs/toolkit';
import { dataOperations } from './index';
import { Data } from './types';

const initialState: Data = {
  teams: null,
  schools: null,
  facilities: null,
  error: undefined,
};

const dataSlice = createSlice({
  name: 'data',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(dataOperations.fetchTeams.fulfilled, (state, action) => {
      state.teams = action.payload.data.teams.teams;
      state.error = undefined;
    });
    builder.addCase(dataOperations.fetchTeams.rejected, (state, action) => {
      state.error = action.error.message;
    });
    builder.addCase(dataOperations.fetchSchools.fulfilled, (state, action) => {
      state.schools = action.payload.data.schools.schools;
      state.error = undefined;
    });
    builder.addCase(dataOperations.fetchSchools.rejected, (state, action) => {
      state.error = action.error.message;
    });
    builder.addCase(dataOperations.fetchFacilities.fulfilled, (state, action) => {
      state.facilities = action.payload.data.facilities.facilities;
      state.error = undefined;
    });
    builder.addCase(dataOperations.fetchFacilities.rejected, (state, action) => {
      state.error = action.error.message;
    });
  },
});

export default dataSlice.reducer;
