import React from 'react';
import { Link } from 'react-router-dom';
import { useTable } from 'react-table';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faHeart as faHeartRegular } from '@fortawesome/free-regular-svg-icons';
import { Team, UpdateFavoriteProfileResponse } from 'src/types';
import { AxiosResponse } from 'axios';
import { UseAsyncReturn } from 'react-async-hook';
import './styles.scss';

interface Column {
  Header: string;
  accessor: string;
}

type HandleChangeStatusFavoriteType = UseAsyncReturn<
  AxiosResponse<UpdateFavoriteProfileResponse>,
  [id: string, favorite: boolean]
>;

interface TableProps {
  columns: Array<Column>;
  data: any[];
  onChangeStatusFavorite?: HandleChangeStatusFavoriteType;
}

const Table = ({
  columns,
  data,
  onChangeStatusFavorite,
}: TableProps): JSX.Element => {
  const tableInstance = useTable({ columns, data });

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = tableInstance;

  return (
    <table {...getTableProps()} className="table-data">
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr
            {...headerGroup.getHeaderGroupProps()}
            key={headerGroup.Header + headerGroup.id}>
            {headerGroup.headers.map((column) => (
              <th
                {...column.getHeaderProps()}
                key={column.Header + column.id}
                className="table-data__header">
                {column.render('Header')}
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, index) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()} key={row.id} className="table-data__row">
              {row.cells.map((cell) => {
                if (cell.column.Header === 'Rank') {
                  return (
                    <td
                      className="table-data__coll"
                      {...cell.getCellProps()}
                      key={cell.column.Header + row.id}>
                      {++row.index}
                    </td>
                  );
                }

                switch (cell.column.id) {
                  case 'batter_name':
                    const { batter_datraks_id } = data[index];
                    return (
                      <td
                        className="table-data__coll"
                        {...cell.getCellProps()}
                        key={cell.column.Header + row.id}>
                        <Link
                          className="table-data__coll--link"
                          to={`/profile/${batter_datraks_id}`}>
                          {cell.value || '-'}
                        </Link>
                      </td>
                    );
                  case 'pitcher_name':
                    const { pitcher_datraks_id } = data[index];
                    return (
                      <td
                        className="table-data__coll"
                        {...cell.getCellProps()}
                        key={cell.column.Header + row.id}>
                        <Link
                          className="table-data__coll--link"
                          to={`/profile/${pitcher_datraks_id}`}>
                          {cell.value || '-'}
                        </Link>
                      </td>
                    );

                  case 'first_name':
                    const { first_name, last_name, id } = data[index];
                    return (
                      <td
                        className="table-data__coll"
                        {...cell.getCellProps()}
                        key={cell.column.Header + row.id}>
                        <Link
                          className="table-data__coll--link"
                          to={`/profile/${id}`}>
                          {first_name + last_name || '-'}
                        </Link>
                      </td>
                    );
                  case 'favorite':
                    const {
                      favorite,
                      batter_datraks_id: idBatter,
                      pitcher_datraks_id: idPitcher,
                      id: idPlayer,
                    } = data[index];

                    return (
                      <td
                        className="table-data__coll"
                        {...cell.getCellProps()}
                        key={cell.column.Header + row.id}>
                        {favorite ? (
                          <a
                            href="#!"
                            className="table-data__coll-link"
                            onClick={() =>
                              onChangeStatusFavorite &&
                              onChangeStatusFavorite?.execute(
                                idBatter || idPitcher || idPlayer,
                                favorite,
                              )
                            }>
                            <FontAwesomeIcon icon={faHeart} color="#48bbff" />
                          </a>
                        ) : (
                          <a
                            href="#!"
                            className="table-data__coll-link"
                            onClick={() =>
                              onChangeStatusFavorite &&
                              onChangeStatusFavorite?.execute(
                                idBatter || idPitcher || idPlayer,
                                favorite,
                              )
                            }>
                            <FontAwesomeIcon
                              icon={faHeartRegular}
                              color="#48bbff"
                            />
                          </a>
                        )}
                      </td>
                    );
                  case 'school':
                    const { school } = data[index];
                    return (
                      <td
                        className="table-data__coll"
                        {...cell.getCellProps()}
                        key={cell.column.Header + row.id}>
                        {school !== null ? school.name : '-'}
                      </td>
                    );

                  case 'teams':
                    const { teams } = data[index];
                    return (
                      <td
                        className="table-data__coll"
                        {...cell.getCellProps()}
                        key={cell.column.Header + row.id}>
                        {teams.length === 0
                          ? '-'
                          : teams.map((team: Team) => team.name).join(',')}
                      </td>
                    );
                  default:
                    return (
                      <td
                        className="table-data__coll"
                        {...cell.getCellProps()}
                        key={cell.column.Header + row.id}>
                        {cell.value || '-'}
                      </td>
                    );
                }
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default Table;
