import { LoadingType } from 'src/types';
import { PlayerProfile } from '../data/types';

const UPDATE_PLAYER_PROFILE = 'playerProfile/updateProfile';
const GET_PLAYER_PROFILE = 'playerProfile/getPlayerProfile';

export interface PlayerProfileState {
  playerProfile: null | PlayerProfile;
  loadingPlayerProfile: LoadingType;
  error: undefined | string;
}

export interface ResponsePlayerProfile {
  data: {
    profile: PlayerProfile;
  };
}

export default {
  UPDATE_PLAYER_PROFILE,
  GET_PLAYER_PROFILE,
}
