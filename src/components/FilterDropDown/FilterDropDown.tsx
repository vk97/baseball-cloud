import React, { useState } from 'react';
import DropDownItem from '../DropDownItem';
import IconArrowDown from '../Icons/IconArrowDown';
import './styles.scss';

interface FilterDropDownProps {
  nameFilter: string;
  renderNameFilter?: () => string;
  activeFilter: string;
  filterLists: Array<string>;
  onChangeFilter: (value: string) => void;
  classElement?: string;
}

const FilterDropDown = ({
  nameFilter,
  activeFilter,
  filterLists,
  onChangeFilter,
  renderNameFilter,
  classElement,
}: FilterDropDownProps): JSX.Element => {
  const [isOpen, setOpen] = useState(false);

  return (
    <div className={`${classElement + ' '}filter-dropdown`}>
      <button
        className="filter-dropdown__button"
        aria-label={`Change filter`}
        onClick={() => setOpen(!isOpen)}>
        <span className="filter-dropdown__name">
          {renderNameFilter !== undefined
            ? renderNameFilter()
            : activeFilter
            ? activeFilter
            : nameFilter}
        </span>
        <span className="filter-dropdown__icon">
          <IconArrowDown width={16} height={9} color={'#48bbff'} />
        </span>
        {isOpen && (
          <DropDownItem itemLists={filterLists} onClick={onChangeFilter} />
        )}
      </button>
    </div>
  );
};

FilterDropDown.defaultProps = {
  renderNameFilter: undefined,
  classElement: '',
} as Partial<FilterDropDownProps>;

export default FilterDropDown;
