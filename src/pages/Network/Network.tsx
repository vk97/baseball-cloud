import React, { useEffect, useMemo, useState } from 'react';
import { Form, Field } from 'react-final-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAngleDoubleLeft,
  faAngleDoubleRight,
} from '@fortawesome/free-solid-svg-icons';
import Select from 'react-select';
import { toast } from 'react-toastify';
import EmptyData from 'src/components/EmptyData';
import FilterInput from 'src/components/FilterInput';
import LoaderData from 'src/components/LoaderData';
import MainContainer from 'src/components/MainContainer';
import ReactPaginate from 'react-paginate';
import Table from 'src/components/Table';
import { POSITION_VALUES, QUERY_PROFILES_NETWORK } from 'src/const';
import useDebounce from 'src/hooks/useDebounce';
import { customStylesSelect } from 'src/styles/customStyles';
import { LoadingType } from 'src/types';
import { fetchApi } from 'src/utils/fetchApi';
import { getFilterValue } from 'src/utils/utils';
import { FilterNetworkType, ProfilesNetwork, ProfilesRequest } from './types';
import './styles.scss';
import { useAsyncCallback } from 'react-async-hook';
import FetchData from 'src/utils/FetchData';

const Network = (): JSX.Element => {
  const [profiles, setProfiles] = useState<ProfilesNetwork[] | []>([]);
  const [loading, setLoading] = useState<LoadingType>('idle');
  const [totalCount, setTotalCount] = useState<number>(0);
  const [searchFilters, setSearchFilters] = useState<FilterNetworkType>({
    profiles_count: 10,
    offset: 0,
  });
  const lengthPagination = Math.ceil(totalCount / searchFilters.profiles_count);
  const columnsNetworkTable = useMemo(
    () => [
      { Header: 'Player Name', accessor: 'first_name' },
      { Header: 'Sessions', accessor: 'sessions' },
      { Header: 'School', accessor: 'school' },
      { Header: 'Teams', accessor: 'teams' },
      { Header: 'Age', accessor: 'age' },
      { Header: 'Favorite', accessor: 'favorite' },
    ],
    [],
  );

  const fetchNetworkProfile = async (filterData: FilterNetworkType) => {
    const data = {
      query: QUERY_PROFILES_NETWORK,
      variables: {
        input: filterData,
      },
    };
    setProfiles([]);
    setLoading('pending');
    const response = await fetchApi<ProfilesRequest>('/api/v1/graphql', data);
    if (response.status === 200) {
      const profiles = response.data.data.profiles.profiles;
      const totalCount = response.data.data.profiles.total_count;
      setProfiles(profiles);
      setTotalCount(totalCount);
      setLoading('succeeded');
    } else {
      setLoading('failed');
      toast.error(`Error request.`);
    }
  };

  const handleUpdateFavorite = useAsyncCallback(
    (id: string, favorite: boolean) => {
      return FetchData.fetchUpdateFavorite(id, !favorite);
    },
    {
      onSuccess(result) {
        if (result.status === 200 && result.data.data.update_favorite_profile) {
          fetchNetworkProfile(searchFilters);
          const isFavorite = result.data.data.update_favorite_profile.favorite;
          toast.success(
            `This profile ${
              isFavorite ? 'added' : 'removed'
            } to favorite list successfully.`,
          );
        }
      },
    },
  );

  const debouncedSearchPlayer = useDebounce(searchFilters, 500);

  useEffect(() => {
    if (debouncedSearchPlayer) {
      fetchNetworkProfile(debouncedSearchPlayer);
    }

    return () => {
      setProfiles([]);
    };
  }, [debouncedSearchPlayer]);

  return (
    <MainContainer>
      <section className="network">
        <div className="network__header">
          <h2 className="network__title">Network</h2>
          <Form
            initialValues={searchFilters}
            onSubmit={(filterData: FilterNetworkType) =>
              setSearchFilters(filterData)
            }
            render={({ handleSubmit, values }) => (
              <form className="form-filters" onSubmit={handleSubmit}>
                <Field
                  name="school"
                  type="text"
                  render={({ input }) => (
                    <FilterInput
                      restProps={{ ...input }}
                      minLength={3}
                      maxLength={35}
                      placeholder="School"
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                    />
                  )}
                />
                <Field
                  name="team"
                  type="text"
                  render={({ input }) => (
                    <FilterInput
                      restProps={{ ...input }}
                      minLength={3}
                      maxLength={35}
                      placeholder="Team"
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                    />
                  )}
                />
                <Field
                  name="position"
                  type="text"
                  placeholder="Position"
                  parse={getFilterValue}
                  render={({ input, placeholder }) => (
                    <Select
                      styles={customStylesSelect}
                      options={POSITION_VALUES}
                      placeholder={placeholder}
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                      isSearchable={false}
                    />
                  )}
                />
                <Field
                  name="age"
                  type="text"
                  parse={(value) => value ? value : undefined}
                  render={({ input }) => (
                    <FilterInput
                      restProps={{ ...input }}
                      minLength={1}
                      maxLength={3}
                      placeholder="Age"
                      onChange={(value) => {
                        const age = Number(value);
                        if (!isNaN(age)) {
                          input.onChange(age);
                          handleSubmit();
                        }
                      }}
                    />
                  )}
                />

                <Field
                  name="favorite"
                  type="text"
                  parse={(option) => (!option.value ? undefined : option.value)}
                  render={({ input }) => (
                    <Select
                      styles={customStylesSelect}
                      options={[
                        { value: '', label: 'All' },
                        { value: 1, label: 'Favorite' },
                      ]}
                      placeholder="All"
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                      isSearchable={false}
                    />
                  )}
                />

                <Field
                  name="profiles_count"
                  type="text"
                  parse={(option) => (!option.value ? undefined : option.value)}
                  render={({ input }) => (
                    <Select
                      styles={customStylesSelect}
                      options={[
                        { value: 10, label: '10' },
                        { value: 15, label: '15' },
                        { value: 25, label: '25' },
                      ]}
                      placeholder={`Show: ${values.profiles_count}`}
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                      isSearchable={false}
                    />
                  )}
                />

                <Field
                  name="player_name"
                  type="text"
                  render={({ input }) => (
                    <div className="network__input-search input-search">
                      <div className="input-search__container input-search__container--network">
                        <input
                          className="input-search__input"
                          {...input}
                          onChange={(value) => {
                            input.onChange(value);
                            handleSubmit();
                          }}
                          placeholder="Player Name"
                        />
                      </div>
                    </div>
                  )}
                />
              </form>
            )}
          />
        </div>
        <section className="network__available-players available-players">
          <h2 className="available-players__title">
            Available Players ({totalCount ? totalCount : '?'})
          </h2>

          <Table
            onChangeStatusFavorite={handleUpdateFavorite}
            columns={columnsNetworkTable}
            data={useMemo(() => profiles, [profiles])}
          />
          {loading === 'pending' && <LoaderData />}
          {profiles.length === 0 && loading === 'succeeded' && <EmptyData />}
        </section>

        {(profiles.length > 0 || lengthPagination !== 0) && (
          <ReactPaginate
            previousLabel={
              <FontAwesomeIcon
                icon={faAngleDoubleLeft}
                color="#667784"
                width="6"
                height="6"
              />
            }
            nextLabel={
              <FontAwesomeIcon
                icon={faAngleDoubleRight}
                color="#667784"
                width="6"
                height="6"
              />
            }
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={lengthPagination}
            marginPagesDisplayed={1}
            pageRangeDisplayed={3}
            onPageChange={({ selected }) =>
              setSearchFilters({
                ...searchFilters,
                offset: selected * searchFilters.profiles_count,
              })
            }
            containerClassName="pagination"
            pageClassName="page-item"
            pageLinkClassName="page-link"
            activeClassName={'active'}
          />
        )}
      </section>
    </MainContainer>
  );
};

export default Network;
