import React from 'react';
import { InputModification } from 'src/types';
import './styles.scss';

interface InputProps {
  children?: React.ReactNode;
  variant?: InputModification;
  [x: string]: any;
}

const Input = ({
  children,
  variant,
  ...inputProperty
}: InputProps): JSX.Element => {
  let classModification;

  if (variant === InputModification.PROFILE) {
    classModification = 'input--profile';
  }
  return (
    <>
      {children}
      <input className={`input ${classModification}`} {...inputProperty} />
    </>
  );
};

Input.defaultProps = {
  variant: 'BASE',
} as Partial<InputProps>;

export default Input;
