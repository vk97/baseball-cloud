import { createAsyncThunk } from '@reduxjs/toolkit';
import { QUERY_PROFILE } from 'src/const';
import { FetchApi } from 'src/types';
import { RootState } from '../reducer';
import { AppDispatch } from '../store';
import { ResponsePlayerProfileError } from '../user/types';
import { ResponsePlayerProfile } from './types';
import playerProfileActionTypes from './types';

const fetchPlayerProfile = createAsyncThunk<
  ResponsePlayerProfile,
  string,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: FetchApi<ResponsePlayerProfile | ResponsePlayerProfileError>;
    rejectValue: ResponsePlayerProfileError;
  }
>(playerProfileActionTypes.UPDATE_PLAYER_PROFILE, async (playerId, thunkApi) => {
  const data = {
    query: QUERY_PROFILE,
    variables: {
      id: playerId,
    },
  };

  const response = await thunkApi.extra('/api/v1/graphql', data);

  if (response.status === 422) {
    return thunkApi.rejectWithValue(
      (await response.data) as ResponsePlayerProfileError,
    );
  }

  return response.data as ResponsePlayerProfile;
});

export default {
  fetchPlayerProfile,
};
