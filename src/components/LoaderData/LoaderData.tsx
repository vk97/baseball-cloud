import React from 'react';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import './styles.scss';

const LoaderData = (): JSX.Element => {
  return (
    <div className="loader">
      <Loader
        type="ThreeDots"
        color="#48bbff"
        height={80}
        width={80}
        timeout={3000}
      />
    </div>
  );
};

export default LoaderData;
