import React from 'react';
import './styles.scss';

const Footer: React.FC = () => {
  return (
    <footer className="page-footer">
      <section className="copyright">
        <p className="copyright__text">© 2018 BaseballCloud</p>
      </section>
      <section className="info-data">
        <a href="#!" className="info-data__link">
          Terms of Service
        </a>
        <a href="#!" className="info-data__link">
          Privacy Policy
        </a>
      </section>
      <section className="social">
        <ul className="social-list">
          <li className="social-list__item">
            <a href="https://baseballcloud.blog/" className="social-list_link">
              Blog
            </a>
          </li>
          <li className="social-list__item">
            <a
              href="http://twitter.com/baseballcloudus"
              className="social-list__link">
              Twitter
            </a>
          </li>
          <li className="social-list__item">
            <a
              href="http://www.instagram.com/baseballcloudus/"
              className="social-list__link">
              Instagram
            </a>
          </li>
          <li className="social-list__item">
            <a
              href="http://www.facebook.com/BaseballCloudUS/"
              className="social-list__link">
              Facebook
            </a>
          </li>
        </ul>
      </section>
    </footer>
  );
};

export default Footer;
