import React from 'react';
import { ProgressBar } from 'react-bootstrap';
import Card from 'src/components/Card';
import { BattingSummary, BattingValue } from 'src/types';
import './styles.scss';

interface TopBattingValuesProps {
  battingValues: BattingSummary | undefined;
}

const TopBattingValues = ({
  battingValues = {
    average_values: [],
    top_values: [],
  },
}: TopBattingValuesProps): JSX.Element => {
  const topExitVelocity =
    battingValues.top_values.length > 0
      ? (battingValues.top_values as BattingValue[]).reduce(
          (acc: BattingValue, curr: BattingValue) =>
            acc.exit_velocity > curr.exit_velocity ? acc : curr,
        ).exit_velocity
      : 0;
  const topDistance =
    battingValues.top_values.length > 0
      ? (battingValues.top_values as BattingValue[]).reduce(
          (acc: BattingValue, curr: BattingValue) =>
            acc.distance > curr.distance ? acc : curr,
        ).distance
      : 0;
  const topLaunchAngle =
    battingValues.top_values.length > 0
      ? (battingValues.top_values as BattingValue[]).reduce(
          (acc: BattingValue, curr: BattingValue) =>
            acc.launch_angle > curr.launch_angle ? acc : curr,
        ).launch_angle
      : 0;
  return (
    <Card classElement="pitcher-summary" title="Top Batting Values">
      <ul className="pitcher-summary__list pitcher-summary-list">
        <li className="pitcher-summary-list__item">
          <span className="pitcher-summary-list__item-title">
            Exit Velocity
          </span>
          <span className="pitcher-summary-list__item-value">
            {topExitVelocity || 'N/A'}
          </span>
          <ProgressBar variant="warning" now={topExitVelocity} />
        </li>
        <li className="pitcher-summary-list__item">
          <span className="pitcher-summary-list__item-title">
            Carry Distance
          </span>
          <span className="pitcher-summary-list__item-value">
            {topDistance || 'N/A'}
          </span>
          <ProgressBar variant="warning" now={topDistance} />
        </li>
        <li className="pitcher-summary-list__item">
          <span className="pitcher-summary-list__item-title">Launch Angle</span>
          <span className="pitcher-summary-list__item-value">
            {topLaunchAngle || 'N/A'}
          </span>
          <ProgressBar variant="warning" now={topLaunchAngle} />
        </li>
      </ul>
    </Card>
  );
};

export default TopBattingValues;
