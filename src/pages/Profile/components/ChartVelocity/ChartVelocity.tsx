import React, { useEffect, useState } from 'react';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import FetchData from 'src/utils/FetchData';
import { useAsyncCallback } from 'react-async-hook';
import useDebounce from 'src/hooks/useDebounce';
import Select from 'react-select';
import { Form, Field } from 'react-final-form';
import { customStylesSelect } from 'src/styles/customStyles';
import { getFilterValue } from 'src/utils/utils';
import LoaderData from 'src/components/LoaderData';
import './styles.scss';
import EmptyData from 'src/components/EmptyData';

interface ChartVelocityProps {
  profileName: string;
  profileId: string;
  variantChart: 'Batting' | 'Pithing';
}

interface Filters {
  pitch_type?: string;
}

const ChartVelocity = ({
  profileName,
  profileId,
  variantChart,
}: ChartVelocityProps): JSX.Element => {
  const [filters, setFilters] = useState<Filters>({});
  const [graphs, setGraphs] = useState<number[]>([]);
  const debouncedChangeFilter = useDebounce(filters, 500);

  const handleGetBattingGraphs = useAsyncCallback(
    (filterEvents) => {
      setGraphs([]);
      return FetchData.fetchBattingGraph(profileId, filterEvents);
    },
    {
      onSuccess: (result) => {
        if (result.data.data) {
          setGraphs(result.data.data.batting_graph.graph_rows);
        }
      },
    },
  );

  const handleGetPithingGraphs = useAsyncCallback(
    (filterEvents) => {
      setGraphs([]);
      return FetchData.fetchPithingGraph(profileId, filterEvents);
    },
    {
      onSuccess: (result) => {
        if (result.data.data) {
          setGraphs(result.data.data.pitching_graph.graph_rows);
        }
      },
    },
  );

  useEffect(() => {
    if (debouncedChangeFilter) {
      if (variantChart === 'Batting') {
        handleGetBattingGraphs.execute(filters);
      } else {
        handleGetPithingGraphs.execute(filters);
      }
    }
  }, [debouncedChangeFilter]);

  const options: Highcharts.Options = {
    title: {
      text: `Rolling Velocity for ${profileName}`,
    },
    series: [
      {
        name: 'Velocity',
        type: 'line',
        data: graphs,
      },
    ],
  };
  const optionsType = [
    { value: '', label: 'None' },
    { value: 'Four Seam Fastball', label: 'Four Seam Fastball' },
    { value: 'Two Seam Fastball', label: 'Two Seam Fastball' },
    { value: 'Curveball', label: 'Curveball' },
    { value: 'Changeup', label: 'Changeup' },
    { value: 'Slider', label: 'Slider' },
  ];

  return (
    <>
      <Form
        onSubmit={(filters: Filters) => setFilters(filters)}
        render={({ handleSubmit }) => (
          <form className="form-chart-filters" onSubmit={handleSubmit}>
            <Field
              name="pitch_type"
              type="text"
              parse={getFilterValue}
              render={({ input }) => (
                <Select
                  styles={customStylesSelect}
                  options={optionsType}
                  placeholder="Pitch Type"
                  onChange={(value) => {
                    input.onChange(value);
                    handleSubmit();
                  }}
                  isSearchable={false}
                />
              )}
            />
          </form>
        )}
      />
      {(handleGetBattingGraphs.loading || handleGetPithingGraphs.loading) && (
        <LoaderData />
      )}
      {!handleGetBattingGraphs.loading &&
        !handleGetPithingGraphs.loading &&
        graphs.length > 0 && (
          <HighchartsReact highcharts={Highcharts} options={options} />
        )}
      {!handleGetBattingGraphs.loading &&
        !handleGetPithingGraphs.loading &&
        graphs.length === 0 && <EmptyData />}
    </>
  );
};

export default ChartVelocity;
