import React from 'react';
import { Field, Form } from 'react-final-form';
import FieldButton from 'src/components/FieldButton';
import Select from 'react-select';
import AsyncSelect from 'react-select/async';
import AsyncCreatableSelect from 'react-select/async-creatable';
import Input from 'src/components/Input';
import {
  ButtonType,
  Facility,
  InputModification,
  School,
  Team,
} from 'src/types';
import { required } from 'src/utils/validate';
import baseAvatarUser from 'src/assets/images/user-avatar.png';
import './styles.scss';
import { UserProfile } from 'src/redux/user/types';
import { useDispatch, useSelector } from 'react-redux';
import { fetchApi } from 'src/utils/fetchApi';
import {
  POSITION_VALUES,
  QUERY_FACILITIES,
  QUERY_SCHOOLS,
  QUERY_TEAMS,
} from 'src/const';
import {
  dataSelectors
} from 'src/redux/data';
import { toast } from 'react-toastify';
import { userOperations } from 'src/redux/user';
import { AppDispatch } from 'src/redux/store';
import { convertStringToNumber } from 'src/utils/utils';
import { TeamsResponse } from './types';
import { FacilitiesResponse, PlayerProfile, SchoolsResponse } from 'src/redux/data/types';

interface FormEditProfileProps {
  profile: PlayerProfile;
  onChangeStatusEdit: () => void;
}

type OptionType = {
  value: string;
  label: string;
};

const FormEditProfile = ({
  profile,
  onChangeStatusEdit,
}: FormEditProfileProps): JSX.Element => {
  const dispatch: AppDispatch = useDispatch();
  const teams = useSelector(dataSelectors.selectTeams);
  const schools = useSelector(dataSelectors.selectSchools);
  const facilities = useSelector(dataSelectors.selectFacilities);
  const secondPositionValues = [{ value: 'null', label: '-' }].concat(
    POSITION_VALUES,
  );
  const throwsAndBatsOptions = [
    { value: 'r', label: 'R' },
    { value: 'l', label: 'L' },
  ];
  const schoolYearOptions = [
    { value: 'freshman', label: 'Freshman' },
    { value: 'sophomore', label: 'Sophomore' },
    { value: 'junior', label: 'Junior' },
    { value: 'senior', label: 'Senior' },
    { value: 'none', label: 'None' },
  ];

  const handleGetTeamsOptions = async (
    searchText: string,
  ): Promise<Array<OptionType>> => {
    const data = {
      query: QUERY_TEAMS,
      variables: {
        search: searchText,
      },
    };
    const response = await fetchApi<TeamsResponse>('/api/v1/graphql', data);

    return (response.data.data.teams.teams as Array<Team>).map((team: Team) => {
      return {
        value: team.id,
        label: team.name,
      };
    });
  };

  const handleGetSchoolOptions = async (
    searchText: string,
  ): Promise<Array<OptionType>> => {
    const data = {
      query: QUERY_SCHOOLS,
      variables: {
        search: searchText,
      },
    };
    const response = await fetchApi<SchoolsResponse>('/api/v1/graphql', data);

    return response.data.data.schools.schools.map((school: School) => {
      return {
        value: school.id,
        label: school.name,
      };
    });
  };

  const handleGetFacilityOptions = async (
    searchText: string,
  ): Promise<Array<OptionType>> => {
    const data = {
      query: QUERY_FACILITIES,
      variables: {
        search: searchText,
      },
    };

    const response = await fetchApi<FacilitiesResponse>(
      '/api/v1/graphql',
      data,
    );

    return response.data.data.facilities.facilities.map(
      (facility: Facility) => {
        return {
          value: facility.id,
          label: facility.u_name,
        };
      },
    );
  };

  const handleUpdateProfile = async (profileData: UserProfile) => {
    const resultAction = await dispatch(userOperations.fetchUpdateProfile(profileData));
    if (userOperations.fetchUpdateProfile.fulfilled.match(resultAction)) {
      toast.success('Profile has been updated successfully.');
      onChangeStatusEdit();
    } else {
      if (resultAction.payload) {
        toast.error(`Update failed: ${resultAction.payload.errorMessage}`);
      } else {
        toast.error(`Update failed: ${resultAction.error.message}`);
      }
    }
  };

  return (
    <Form
      initialValues={profile}
      onSubmit={(userProfile: UserProfile) => handleUpdateProfile(userProfile)}
      render={({ handleSubmit, values }) => (
        <form className="user-profile-form" onSubmit={handleSubmit}>
          <div className="user-profile-form__block form-block">
            <div className="form-block__input-group form-block__input-group--avatar">
              <div className="user-profile-form__container-image">
                <img src={baseAvatarUser} alt="User Avatar" />
              </div>
              <Field
                name="avatar"
                type="file"
                id="user-avatar"
                render={({ input }) => (
                  <>
                    <Input
                      className="display-none"
                      variant={InputModification.PROFILE}
                      id="user-avatar"
                      {...input}
                    />
                    <label
                      className="input-label input-label--photo"
                      htmlFor="user-avatar">
                      Choose Photo
                    </label>
                  </>
                )}
              />
            </div>
            <div className="form-block__container form-block__container--fio">
              <div className="form-block__input-group">
                <Field
                  name="first_name"
                  type="text"
                  validate={required}
                  render={({ input, meta }) => (
                    <>
                      <Input
                        placeholder="First Name*"
                        variant={InputModification.PROFILE}
                        id="first-name"
                        {...input}
                      />
                      <label className="input-label" htmlFor="first-name">
                        First Name*
                      </label>
                      {meta.touched && meta.error && (
                        <span className="form-block__text-error">
                          {meta.error}
                        </span>
                      )}
                    </>
                  )}
                />
              </div>
              <div className="form-block__input-group">
                <Field
                  name="last_name"
                  type="text"
                  validate={required}
                  render={({ input, meta }) => (
                    <>
                      <Input
                        placeholder="Last Name*"
                        variant={InputModification.PROFILE}
                        id="last-name"
                        {...input}
                      />
                      <label className="input-label" htmlFor="last-name">
                        Last Name*
                      </label>
                      {meta.touched && meta.error && (
                        <span className="form-block__text-error">
                          {meta.error}
                        </span>
                      )}
                    </>
                  )}
                />
              </div>
            </div>
            <div className="form-block__input-group">
              <Field
                name="position"
                placeholder="Position Game*"
                validate={required}
                render={({ input, meta, placeholder }) => (
                  <>
                    <Select
                      className="input-select-container"
                      classNamePrefix="input-select"
                      name={input.name}
                      placeholder={placeholder}
                      value={POSITION_VALUES.find(
                        (option) => option.value === values.position,
                      )}
                      onChange={(option) => {
                        if (option) {
                          input.onChange(option.value);
                        }
                      }}
                      options={POSITION_VALUES}
                    />
                    {meta.touched && meta.error && (
                      <span className="form-block__text-error">
                        {meta.error}
                      </span>
                    )}
                  </>
                )}
              />
            </div>
            <div className="form-block__input-group">
              <Field
                name="position2"
                placeholder="Secondary Position in Game"
                render={({ input, placeholder }) => (
                  <Select
                    className="input-select-container"
                    classNamePrefix="input-select"
                    name={input.name}
                    placeholder={placeholder}
                    value={secondPositionValues.find(
                      (option) => option.value === values.position2,
                    )}
                    onChange={(option) => {
                      if (option) {
                        input.onChange(option.value);
                      }
                    }}
                    options={secondPositionValues}
                  />
                )}
              />
            </div>
          </div>
          <div className="user-profile-form__block form-block">
            <div className="form-block__header">
              <h2 className="form-block__title">Personal Info</h2>
            </div>
            <div className="form-block__input-group">
              <Field
                name="age"
                type="number"
                validate={required}
                parse={convertStringToNumber}
                render={({ input, meta }) => (
                  <>
                    <Input
                      placeholder="Age*"
                      variant={InputModification.PROFILE}
                      id="age"
                      {...input}
                    />
                    <label className="input-label" htmlFor="age">
                      Age*
                    </label>
                    {meta.touched && meta.error && (
                      <span className="form-block__text-error">
                        {meta.error}
                      </span>
                    )}
                  </>
                )}
              />
            </div>
            <div className="form-block__container">
              <div className="form-block__input-group">
                <Field
                  name="feet"
                  type="number"
                  validate={required}
                  parse={convertStringToNumber}
                  render={({ input, meta }) => (
                    <>
                      <Input
                        placeholder="Feet*"
                        variant={InputModification.PROFILE}
                        id="feet"
                        {...input}
                      />
                      <label className="input-label" htmlFor="feet">
                        Feet*
                      </label>
                      {meta.touched && meta.error && (
                        <span className="form-block__text-error">
                          {meta.error}
                        </span>
                      )}
                    </>
                  )}
                />
              </div>
              <div className="form-block__input-group">
                <Field
                  name="inches"
                  type="number"
                  parse={convertStringToNumber}
                  render={({ input }) => (
                    <>
                      <Input
                        placeholder="Inches"
                        variant={InputModification.PROFILE}
                        id="inches"
                        {...input}
                      />
                      <label className="input-label" htmlFor="inches">
                        Inches
                      </label>
                    </>
                  )}
                />
              </div>
            </div>
            <div className="form-block__input-group">
              <Field
                name="weight"
                type="number"
                validate={required}
                parse={convertStringToNumber}
                render={({ input, meta }) => (
                  <>
                    <Input
                      placeholder="Weight*"
                      variant={InputModification.PROFILE}
                      id="weight"
                      {...input}
                    />
                    <label className="input-label" htmlFor="weight">
                      Weight*
                    </label>
                    {meta.touched && meta.error && (
                      <span className="form-block__text-error">
                        {meta.error}
                      </span>
                    )}
                  </>
                )}
              />
            </div>
            <div className="form-block__container">
              <div className="form-block__input-group">
                <Field
                  name="throws_hand"
                  validate={required}
                  placeholder="Throws*"
                  render={({ input, meta, placeholder }) => (
                    <>
                      <Select
                        className="input-select-container"
                        classNamePrefix="input-select"
                        name={input.name}
                        placeholder={placeholder}
                        value={throwsAndBatsOptions.find(
                          (option) => option.value === values.throws_hand,
                        )}
                        onChange={(option) => {
                          if (option) {
                            input.onChange(option.value);
                          }
                        }}
                        options={throwsAndBatsOptions}
                      />
                      {meta.touched && meta.error && (
                        <span className="form-block__text-error">
                          {meta.error}
                        </span>
                      )}
                    </>
                  )}
                />
              </div>
              <div className="form-block__input-group">
                <Field
                  name="bats_hand"
                  validate={required}
                  placeholder="Bats*"
                  render={({ input, meta, placeholder }) => (
                    <>
                      <Select
                        className="input-select-container"
                        classNamePrefix="input-select"
                        name={input.name}
                        placeholder={placeholder}
                        value={throwsAndBatsOptions.find(
                          (option) => option.value === values.bats_hand,
                        )}
                        onChange={(option) => {
                          if (option) {
                            input.onChange(option.value);
                          }
                        }}
                        options={throwsAndBatsOptions}
                      />
                      {meta.touched && meta.error && (
                        <span className="form-block__text-error">
                          {meta.error}
                        </span>
                      )}
                    </>
                  )}
                />
              </div>
            </div>
          </div>
          <div className="user-profile-form__block form-block">
            <div className="form-block__header">
              <h2 className="form-block__title">School</h2>
            </div>
            <div className="form-block__input-group">
              <Field
                name="school"
                placeholder="School"
                render={({ input, placeholder }) => (
                  <AsyncCreatableSelect
                    className="input-select-container"
                    classNamePrefix="input-select"
                    name={input.name}
                    placeholder={placeholder}
                    value={
                      values.school &&
                      Object.assign(
                        {},
                        {
                          value: values.school.id,
                          label: values.school.name,
                        },
                      )
                    }
                    onChange={(option, action) => {
                      if (schools !== null) {
                        const schoolIds = schools.map((school) =>
                          Number(school.id),
                        );
                        let maxId = Math.max.apply(null, schoolIds);

                        let school;

                        if (action.action === 'create-option') {
                          school = { id: String(++maxId), name: option.label };
                        } else {
                          school = { id: option.value, name: option.label };
                        }

                        input.onChange(school);
                      }
                    }}
                    cacheOptions
                    defaultOptions
                    loadOptions={handleGetSchoolOptions}
                  />
                )}
              />
            </div>
            <div className="form-block__input-group">
              <Field
                name="school_year"
                placeholder="School year"
                render={({ input, placeholder }) => (
                  <Select
                    className="input-select-container"
                    classNamePrefix="input-select"
                    name={input.name}
                    placeholder={placeholder}
                    value={schoolYearOptions.find(
                      (option) => option.value === values.school_year,
                    )}
                    onChange={(option) => {
                      if (option) {
                        input.onChange(option.value);
                      }
                    }}
                    options={schoolYearOptions}
                  />
                )}
              />
            </div>
            <div className="form-block__input-group">
              <Field
                name="teams"
                placeholder="Team"
                render={({ input, placeholder }) => (
                  <AsyncCreatableSelect
                    className="input-select-container"
                    classNamePrefix="input-select"
                    name={input.name}
                    placeholder={placeholder}
                    value={(values.teams as any[]).map(
                      (team: { id: string; name: string }) => {
                        return {
                          value: team.name.toLocaleLowerCase(),
                          label: team.name,
                        };
                      },
                    )}
                    onChange={(options, action) => {
                      if (teams !== null) {
                        const teamIds = teams.map((team) =>
                          team.id ? Number(team.id) : 0,
                        );
                        let maxId = Math.max.apply(null, teamIds);
                        const teamsProfile = options.map((option) => {
                          if (action.action === 'create-option') {
                            return { id: String(++maxId), name: option.label };
                          } else {
                            return { id: option.value, name: option.label };
                          }
                        });

                        input.onChange(teamsProfile);
                      }
                    }}
                    cacheOptions
                    defaultOptions
                    loadOptions={handleGetTeamsOptions}
                    isMulti={true}
                  />
                )}
              />
            </div>
          </div>
          <div className="user-profile-form__block form-block">
            <div className="form-block__header">
              <h2 className="form-block__title">Facility</h2>
            </div>
            <div className="form-block__input-group">
              <Field
                name="facilities"
                placeholder="Facility"
                render={({ input, placeholder }) => (
                  <AsyncSelect
                    className="input-select-container"
                    classNamePrefix="input-select"
                    name={input.name}
                    placeholder={placeholder}
                    onChange={(option) => {
                      if (facilities !== null && option) {
                        const facilitiesFilter = facilities.filter(
                          (facility) => facility.id === option.value,
                        );

                        input.onChange(facilitiesFilter);
                      }
                    }}
                    cacheOptions
                    defaultOptions
                    loadOptions={handleGetFacilityOptions}
                  />
                )}
              />
            </div>
          </div>
          <div className="user-profile-form__block form-block">
            <div className="form-block__header">
              <h2 className="form-block__title">About</h2>
            </div>
            <div className="form-block__input-group">
              <Field
                name="biography"
                render={({ input }) => (
                  <>
                    <textarea
                      placeholder="Describe yourself in a few words"
                      className="input input--area"
                      id="about"
                      {...input}>
                      {values.biography}
                    </textarea>
                    <label className="input-label" htmlFor="about">
                      Describe yourself in a few words
                    </label>
                  </>
                )}
              />
            </div>
          </div>
          <p className="user-profile-form__info">
            * Fill out the required fields
          </p>
          <div className="user-profile-form__buttons form-buttons">
            <FieldButton
              title="Cancel"
              buttonType={ButtonType.TRANSPARENT}
              onClick={onChangeStatusEdit}
            />
            <FieldButton
              title="Save"
              buttonType={ButtonType.SUBMIT}
              onClick={() => undefined}
            />
          </div>
        </form>
      )}
    />
  );
};

export default FormEditProfile;
