import React, { useEffect, useState } from 'react';
import { useAsyncCallback } from 'react-async-hook';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { customHistory } from 'src/App';
import Comparison from 'src/components/Comparison';
import LoaderData from 'src/components/LoaderData';
import MainContainer from 'src/components/MainContainer';
import RecentSessionReports from 'src/components/RecentSessionReports';
import SideBarLeft from 'src/components/SideBarLeft';
import { userSelectors } from 'src/redux/user';
import { LoadingType, ProfileFavorite, TypeAccount } from 'src/types';
import FetchData from 'src/utils/FetchData';
import './styles.scss';

const Dashboard = (): JSX.Element => {
  const [favoriteProfiles, setFavoriteProfiles] = useState<
    ProfileFavorite[] | []
  >([]);
  const [loading, setLoading] = useState<LoadingType>('idle');
  const [totalCount, setTotalCount] = useState<number>(0);
  const userData = useSelector(userSelectors.selectUserData);

  if (userData?.role === TypeAccount.PLAYER) {
    customHistory.push('/profile');
  }

  const handleGetFavoriteProfiles = useAsyncCallback(
    FetchData.fetchFavoriteProfiles,
    {
      onSuccess: (result) => {
        if (result.status === 200) {
          setFavoriteProfiles(result.data.data.my_favorite.profiles);
          setTotalCount(result.data.data.my_favorite.total_count);
          setLoading('succeeded');
        }
      },
    },
  );

  useEffect(() => {
    if (loading === 'idle') {
      handleGetFavoriteProfiles.execute();
    }
    return () => {
      handleGetFavoriteProfiles.reset;
    };
  }, []);

  return (
    <MainContainer>
      <section className="dashboard">
        <SideBarLeft>
          {handleGetFavoriteProfiles.loading && <LoaderData />}
          {favoriteProfiles.length > 0 && (
            <ul className="dashboard__user-favorite user-favorite-lists">
              {(favoriteProfiles as ProfileFavorite[]).map((user, index) => {
                return (
                  <li
                    className="user-favorite-lists__item"
                    key={user.first_name + index}>
                    <Link
                      to={`/profile/${user.id}`}
                      className="user-favorite-lists__link">
                      {user.first_name} {user.last_name}
                    </Link>
                  </li>
                );
              })}
            </ul>
          )}
        </SideBarLeft>
        <section className="dashboard__report">
          <h2 className="visually-hidden">Dashboard Report</h2>
          <RecentSessionReports
            profileList={favoriteProfiles}
            isLoading={handleGetFavoriteProfiles.loading}
          />
          <div className="card">
            <Comparison players={favoriteProfiles} />
          </div>
        </section>
      </section>
    </MainContainer>
  );
};

export default Dashboard;
