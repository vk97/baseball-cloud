import { createAction } from '@reduxjs/toolkit';
import types from 'src/redux/user/types';

export const userLogout = createAction(types.USER_LOGOUT);
