import { AxiosResponse } from 'axios';
import { RequestForgotPassword, UserProfile } from './redux/user/types';

export enum MainLayoutModification {
  BASE = 'BASE',
  AUTHORIZATION = 'AUTHORIZATION',
}

export enum InputModification {
  BASE = 'BASE',
  PROFILE = 'PROFILE',
}

export enum ButtonType {
  SUBMIT = 'SUBMIT',
  TRANSPARENT = 'TRANSPARENT',
  DANGER = 'DANGER',
}

export enum TypeAccount {
  PLAYER = 'player',
  SCOUT = 'scout',
}

export enum TypeDropDown {
  NAVIGATION = 'NAVIGATION',
  BASE = 'BASE',
}

export interface RecentEventType {
  id: string;
  event_type: string;
  event_name: string;
  date: string;
  data_rows_count?: number;
  recent_avatars?: UserProfile[];
}

export interface RecentEventTypeResponse {
  data: {
    profile_events: {
      events: RecentEventType[] | [];
      total_count: number;
    };
  };
}

export type ERROR = string | null;

export type LoadingType = 'idle' | 'pending' | 'succeeded' | 'failed';

export interface UserSignInRequest {
  email: string;
  password: string;
}

export interface UserSignUpRequest {
  email: string;
  password: string;
  password_confirmation: string;
  role: string;
}

export interface Team {
  id: string;
  name: string;
}

export interface School {
  id: string;
  name: string;
}

export interface Facility {
  id: string;
  email: string;
  u_name: string;
}

export type Schools = any | School;
export type Facilities = [] | Array<Facility>;
export type Teams = [] | Array<Team>;

interface QueryRequest {
  query: string;
  variables?: {
    [key: string]: any;
  };
}

export type ApiData =
  | QueryRequest
  | UserSignInRequest
  | UserSignUpRequest
  | RequestForgotPassword;
export interface FetchApi<T> {
  (url: string, data: ApiData): AxiosResponse<T>;
}

export interface BattingValue {
  distance: number;
  exit_velocity: number;
  id: string | null;
  launch_angle: number;
  pitch_type: string;
}

export interface PitchingValue {
  id: null | string;
  pitch_type: string;
  spin_rate: number;
  velocity: number;
}

export interface BattingSummary {
  average_values: BattingValue[] | [];
  top_values: BattingValue[] | [];
}

export interface BattingSummaryResponse {
  data: {
    batting_summary: BattingSummary;
  };
}

export interface PitchingSummary {
  average_values: PitchingValue[] | [];
  top_values: PitchingValue[] | [];
}

export interface PitchingSummaryResponse {
  data: {
    pitching_summary: PitchingSummary;
  };
}

export interface BattingGraph {
  graph_rows: number[];
}

export interface BattingGraphResponse {
  data: {
    batting_graph: BattingGraph;
  };
}

export interface PitcherGraph {
  graph_rows: number[];
}

export interface PitcherGraphResponse {
  data: {
    pitching_graph: PitcherGraph;
  };
}

export interface BattingLog {
  date: string;
  direction: null | number;
  distance: null | number;
  exit_velocity: null | number;
  hang_time: null | number;
  hit_spin_rate: null | number;
  launch_angle: null | number;
  pitch_call: string;
  pitch_type: string;
  pitcher_datraks_id: null | number;
  pitcher_handedness: string;
  pitcher_name: string;
}

export interface PitcherLog {
  batter_datraks_id: number;
  batter_handedness: string;
  batter_name: string;
  date: string;
  extension: string;
  height_at_plate: null | number;
  horizontal_break: null;
  pitch_call: string;
  pitch_type: string;
  release_height: number;
  release_side: number;
  spin_axis: null | number;
  spin_rate: number;
  tilt: null | number;
  velocity: number;
  vertical_break: null | number;
}

export interface BattingLogResponse {
  data: {
    batting_log: {
      batting_log: BattingLog[];
      total_count: number;
    };
  };
}

export interface PithingLogResponse {
  data: {
    pitching_log: {
      pitching_log: BattingLog[];
      total_count: number;
    };
  };
}

export interface ProfileName {
  age: number;
  feet: number;
  first_name: string;
  id: string;
  inches: number;
  last_name: string;
  position: string;
  weight: number;
}

export interface ProfileNamesResponse {
  data: {
    profile_names: {
      profile_names: ProfileName[];
    };
  };
}

export interface ProfileFavorite {
  first_name: string;
  id: string;
  last_name: string;
  recent_events: RecentEventType[];
}

export interface ProfileFavoriteResponse {
  data: {
    my_favorite: {
      profiles: ProfileFavorite[];
      total_count: number;
    };
  };
}

export interface UpdateFavoriteProfileResponse {
  data: {
    update_favorite_profile: {
      favorite: boolean;
    };
  };
}

export interface ProfilesInEvent {
  age: number;
  events: { id: string }[];
  favorite: boolean;
  feet: number;
  first_name: string;
  id: string;
  inches: number | null;
  last_name: string;
  position: string;
  position2: string;
  school: School;
  school_year: string;
  teams: Teams;
  weight: number;
}

export interface ProfilesInEventResponse {
  data: {
    profiles: {
      profiles: ProfilesInEvent[];
    };
  };
}

export interface EventDetail {
  data_rows_count: number;
  event: RecentEventType;
  is_pitcher: boolean;
  opened: boolean;
  profile: string;
}

export interface EventDetailResponse {
  data: {
    event_detail: EventDetail;
  };
}

export interface HittingSummaryTableRow {
  direction: number | null;
  distance: string;
  exit_velocity: string;
  launch_angle: number | null;
  pitch_call: number | null;
  pitch_type: number | null;
}

export interface HittingZoneChartRows {
  height_at_plate: null | number;
  pitch_type: null | number;
  side_at_plate: null | number;
}

export interface EventPlayerSummary {
  hitter_unique_pitches: [];
  hitting_summary_table_rows: HittingSummaryTableRow[];
  hitting_zone_chart_rows: HittingZoneChartRows[];
  pitcher_unique_pitches: [];
  pitching_summary_table_rows: [];
  pitching_zone_chart_rows: [];
  release_point_chart_rows: [];
}

export interface EventPlayerSummaryResponse {
  data: {
    event_player_summary: EventPlayerSummary;
  };
}

export interface EventRow {
  batter_name: string;
  direction: null | number;
  distance: number;
  exit_velocity: number;
  extension: null | number;
  hang_time: number;
  height_at_plate: null | number;
  hit_spin_rate: null | number;
  horizontal_break: null | number;
  id: string;
  launch_angle: null | number;
  pitch_call: null | number;
  pitch_type: null | number;
  pitcher_handedness: string;
  pitcher_name: string;
  release_side: null | number;
  spin_rate: null | number;
  velocity: null | number;
  vertical_break: null | number;
}

export interface EventRowsResponse {
  data: {
    event_rows: {
      event_rows: EventRow[];
      total_count: number;
    };
  };
}

export interface HittingSummaryRow {
  batter_name: string;
  distance: number;
  exit_velocity: number;
  launch_angle: null | number;
  pitch_call: null | number;
  pitch_type: null | number;
  pitcher_name: string;
}

export interface EventHittingSummaryRowsResponse {
  data: {
    event_hitting_summary: {
      hitting_summary_rows: HittingSummaryRow[];
    };
  };
}

export interface PitchingSummaryRow {
  avg_spin: string;
  avg_velo: string;
  ball_count: number;
  called_strike_count: number;
  max_spin: string;
  max_velo: string;
  pitch_count: 1;
  pitch_type: string;
  pitcher_name: string;
  pitcher_team: string;
  swing_strike_count: number;
}

export interface VelocityChartRows {
  pitch_type: string;
  pitcher_name: string;
  pitcher_team: string;
  velo: string;
}

export interface EventPitchingSummaryResponse {
  data: {
    event_pitching_summary: {
      pitching_summary_rows: PitchingSummaryRow[];
      velocity_chart_rows: VelocityChartRows[];
    };
  };
}
