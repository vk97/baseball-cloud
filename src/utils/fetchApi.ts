import { AxiosResponse } from 'axios';
import { ApiData } from 'src/types';
import createAPI from './api';

export const fetchApi = async <T>(url: string, data: ApiData): Promise<AxiosResponse<T>> => {
  const api = createAPI();

  return await api.post(url, data);
};
