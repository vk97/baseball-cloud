import React from 'react';
import { useSelector } from 'react-redux';
import { Router, Switch, Route, Redirect } from 'react-router';
import { customHistory } from 'src/App';
import PrivateRoute from 'src/components/PrivateRoute';
import { userSelectors } from 'src/redux/user';
import {
  Leaderboard,
  ProfileEvents,
  Dashboard,
  Network,
  SignIn,
  SignUp,
  ForgotPassword,
  NotFound,
  Profile
} from '..';

const Navigation = () => {
  const userData = useSelector(userSelectors.selectUserData);
  const token = useSelector(userSelectors.selectToken);

  return (
    <Router history={customHistory}>
      <Switch>
        <PrivateRoute
          exact
          path="/"
          render={() => {
            return <Leaderboard />;
          }}
        />

        <PrivateRoute
          exact
          path="/profile"
          render={(props) => <Profile {...props} />}
        />
        <PrivateRoute
          exact
          path="/profile/:id"
          render={(props) => <Profile {...props} />}
        />
        <PrivateRoute
          exact
          path="/profile/:idProfile/event/:idEvent"
          render={(props) => <ProfileEvents {...props} />}
        />
        <PrivateRoute exact path="/dashboard" render={() => <Dashboard />} />
        <PrivateRoute
          exact
          path="/leaderboard"
          render={() => <Leaderboard />}
        />
        <PrivateRoute exact path="/network" render={() => <Network />} />
        <Route
          exact
          path="/login"
          render={() => {
            return token !== null && userData !== null ? (
              <Redirect to="/" />
            ) : (
              <SignIn />
            );
          }}
        />
        <Route exact path="/registration" component={SignUp} />
        <Route exact path="/forgotpassword" component={ForgotPassword} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
};

export default Navigation;
