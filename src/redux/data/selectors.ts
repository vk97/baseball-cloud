import { Facility, School, Team } from 'src/types';
import { RootState } from '../reducer';

const selectTeams = (state: RootState): Array<Team> | null =>
  state.DATA.teams;
const selectSchools = (state: RootState): Array<School> | null =>
  state.DATA.schools;
const selectFacilities = (state: RootState): Array<Facility> | null =>
  state.DATA.facilities;
const selectErrorData = (state: RootState): undefined | string =>
  state.DATA.error;

export default {
  selectTeams,
  selectSchools,
  selectFacilities,
  selectErrorData,
}
