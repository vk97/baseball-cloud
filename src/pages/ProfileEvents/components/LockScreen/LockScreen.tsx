import React from 'react';
import { Link } from 'react-router-dom';
import IconLock from 'src/components/Icons/IconLock';
import { EventDetail } from 'src/types';
import './styles.scss';

interface LockScreenProps {
  eventDetail: EventDetail;
}

const LockScreen = ({ eventDetail }: LockScreenProps): JSX.Element => {
  return (
    <section className="lock-screen">
      <div className="lock-screen__container">
        <div className="lock-screen__icon">
          <IconLock color="#ffffff" width={65} height={65} />
        </div>
        <p className="lock-screen__text lock-screen__text--header">
          {eventDetail.profile} has {eventDetail.data_rows_count} pitches seen
          in this session.
        </p>
        <p className="lock-screen__text">
          Each report is personalized for each player in the session.
        </p>
        <p className="lock-screen__text">
          Please&nbsp;
          <Link className="lock-screen__link" to="/preference">
            upgrade your subscription
          </Link>
          &nbsp;to view data from {eventDetail.event.event_name} on
          {eventDetail.event.date} for {eventDetail.profile}.
        </p>

        <footer className="lock-screen__footer lock-footer">
          <p className="lock-footer__text">
            Curious as to what is in the report?
          </p>
          <a
            href="https://s3.us-east-2.amazonaws.com/baseballcloud.com/BaseballCloud_paid_event_report_sample.pdf"
            className="lock-footer__button">
            Download a sample
          </a>
        </footer>
      </div>
    </section>
  );
};

export default LockScreen;
