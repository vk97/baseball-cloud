import React from 'react';
import './styles.scss';

const EmptyData = (): JSX.Element => {
  return <p className="empty-data-text">There s no info yet!</p>;
};

export default EmptyData;
