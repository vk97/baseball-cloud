import { createAction } from '@reduxjs/toolkit';
import { playerProfileActionTypes } from 'src/redux/playerProfile';

const getPlayerProfile = createAction<number>(
  playerProfileActionTypes.GET_PLAYER_PROFILE,
);

export default {
  getPlayerProfile,
};
