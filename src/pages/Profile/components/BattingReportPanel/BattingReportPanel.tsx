import React from 'react';
import { BattingSummary } from 'src/types';
import ChartVelocity from '../ChartVelocity';
import { VariantBattingAndPitchingType } from '../UserProfileReport/types';
import LogTable from '../LogTable';
import BattingAndPitchingSummary from '../BattingAndPitchingSummary';
import LoaderData from 'src/components/LoaderData';
import { PlayerProfile } from 'src/redux/data/types';

interface BattingReportPanelProps {
  userProfile: PlayerProfile;
  variant: VariantBattingAndPitchingType;
  battingSummary: BattingSummary;
}

const BattingReportPanel = ({
  userProfile,
  variant,
  battingSummary,
}: BattingReportPanelProps): JSX.Element => {
  const columnsTableSummary = [
    { Header: 'Pitch Type', accessor: 'pitch_type' },
    { Header: 'Distance', accessor: 'distance' },
    { Header: 'Launch Angle', accessor: 'launch_angle' },
  ];
  const columnsTableLog = [
    { Header: 'Date', accessor: 'date' },
    { Header: 'Pitcher Name', accessor: 'pitcher_name' },
    { Header: 'Pitcher Handedness', accessor: 'pitcher_handedness' },
    { Header: 'Pitch Type', accessor: 'pitch_type' },
    { Header: 'Pitch Call', accessor: 'pitch_call' },
  ];

  if (userProfile?.id) {
    switch (variant) {
      case 'Summary':
        return (
          <BattingAndPitchingSummary
            type="Batting"
            dataSummary={battingSummary}
            columns={columnsTableSummary}
          />
        );
      case 'Charts':
        return (
          <ChartVelocity
            variantChart="Batting"
            profileName={`${userProfile?.first_name} ${userProfile?.last_name}`}
            profileId={userProfile.id}
          />
        );
      case 'Log':
        return (
          <LogTable
            type="Batting"
            columns={columnsTableLog}
            profileId={userProfile.id}
          />
        );
      default:
        return (
          <BattingAndPitchingSummary
            type="Batting"
            dataSummary={battingSummary}
            columns={columnsTableSummary}
          />
        );
    }
  } else {
    return <LoaderData />;
  }
};

export default BattingReportPanel;
