import React from 'react';
import MainContainer from 'src/components/MainContainer';
import './styles.scss';

const NotFound = () => {
  return (
    <MainContainer>
      <section className="page-not-found">
        <h2 className="page-not-found__title">Page Not Found</h2>
        <div className="page-not-found__container">
          <span className="page-not-found__text">4</span>
          <span className="page-not-found__img"></span>
          <span className="page-not-found__text">4</span>
        </div>
      </section>
    </MainContainer>
  );
};

export default NotFound;
