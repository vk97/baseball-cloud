import React from 'react';
import Card from 'src/components/Card';
import './styles.scss';

const HittingStrikeZoneChart = (): JSX.Element => {
  return (
    <Card title="Hitting Strike Zone Chart" titleHide={true}>
      <div></div>
    </Card>
  );
};

export default HittingStrikeZoneChart;
