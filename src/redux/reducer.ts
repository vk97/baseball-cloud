import userReducer from './user/userSlice';
import dataReducer from './data/dataSlice';
import playerReducer from './playerProfile/playerProfileSlice';
import { combineReducers } from 'redux';
import { AnyAction, Reducer } from '@reduxjs/toolkit';
import { NAME_SPACE } from './namespace';
import { userLogout } from './user/actions';

const combinedReducer = combineReducers({
  [NAME_SPACE.USER]: userReducer,
  [NAME_SPACE.DATA]: dataReducer,
  [NAME_SPACE.PLAYER]: playerReducer,
});

export type RootState = ReturnType<typeof combinedReducer>;

const rootReducer: Reducer = (state: RootState, action: AnyAction) => {
  if (action.type === userLogout.toString()) {
    state = {} as RootState;
  }
  return combinedReducer(state, action);
};

export default rootReducer;
