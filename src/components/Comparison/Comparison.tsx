import React, { useEffect, useState } from 'react';
import { Form, Field } from 'react-final-form';
import AsyncSelect from 'react-select/async';
import baseUserImage from 'src/assets/images/user-avatar.png';
import iconSearch from 'src/assets/images/icon-search.svg';
import { PlayerProfile } from 'src/redux/data/types';
import FetchData from 'src/utils/FetchData';
import {
  customStylesInputSelect,
  customStylesSelect,
} from 'src/styles/customStyles';
import ComparisonTable from '../ComparisonTable';
import './styles.scss';
import { checkPositionPitcher } from 'src/utils/utils';
import Select from 'react-select';
import { ProfileFavorite } from 'src/types';

interface ComparisonProps {
  firstProfileId: string;
  players: ProfileFavorite[];
}

type OptionType = {
  value: string;
  label: string;
};

const Comparison = ({
  firstProfileId,
  players,
}: ComparisonProps): JSX.Element => {
  const [firstProfile, setFirstProfile] = useState<PlayerProfile>();
  const [lastProfile, setLastProfile] = useState<PlayerProfile>();
  const [profileFirstSelectedId, setProfileFirstSelectedId] = useState<string>(
    firstProfileId,
  );
  const [profileLastSelectedId, setProfileLastSelectedId] = useState<string>(
    '',
  );
  const isPitcher = firstProfile && checkPositionPitcher(firstProfile);

  const handleGetProfilesName = async (
    inputValues: string,
  ): Promise<Array<OptionType> | []> => {
    if (firstProfile?.position) {
      const response = await FetchData.fetchProfileNames({
        position: firstProfile.position,
        player_name: inputValues,
      });

      return response.data.data.profile_names.profile_names.map((player) => {
        return {
          value: player.id,
          label: `${player.first_name} ${player.last_name}`,
        };
      });
    } else {
      return [];
    }
  };

  const handleGetLastProfile = async () => {
    const response = await FetchData.fetchProfile(profileLastSelectedId);

    if (response.status === 200) {
      setLastProfile(response.data.data.profile);
    }
  };

  const handleGetFirstProfile = async (id: string) => {
    const response = await FetchData.fetchProfile(id);

    if (response.status === 200) {
      setFirstProfile(response.data.data.profile);
    }
  };

  useEffect(() => {
    if (!!firstProfileId) {
      handleGetFirstProfile(firstProfileId);
    } else if (profileFirstSelectedId) {
      handleGetFirstProfile(profileFirstSelectedId);
    }
    if (profileLastSelectedId) {
      handleGetLastProfile();
    }
  }, [profileFirstSelectedId, profileLastSelectedId, firstProfileId]);

  return (
    <section className="comparison">
      <h2 className="comparison__title visually-hidden">Comparison players</h2>
      {!!firstProfileId === true && firstProfile && (
        <>
          <div className="comparison__user-container user-comparison">
            <div className="user-comparison__header">
              <div className="user-comparison__image">
                <img
                  width={40}
                  height={40}
                  src={
                    firstProfile.avatar ? firstProfile.avatar : baseUserImage
                  }
                  alt="User profile image"
                />
              </div>

              <p className="user-comparison__name">
                {`${firstProfile.first_name} ${firstProfile.last_name}`}
              </p>
            </div>
            <div className="user-comparison__user-info">
              <table className="user-data-table">
                <tbody>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Age:</td>
                    <td className="user-data-table__coll">
                      {firstProfile.age}
                    </td>
                  </tr>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Height:</td>
                    <td className="user-data-table__coll">
                      {firstProfile.feet
                        ? `${firstProfile.feet} ft ${
                            firstProfile.inches
                              ? `${firstProfile.inches} in`
                              : '0 in'
                          }`
                        : '-'}
                    </td>
                  </tr>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Weight:</td>
                    <td className="user-data-table__coll">
                      {firstProfile.weight ? `${firstProfile.weight} lbs` : '-'}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className="comparison__user-container user-comparison">
            <div className="user-comparison__header">
              <div className="user-comparison__image">
                <img
                  width={40}
                  height={40}
                  src={
                    firstProfile.avatar ? firstProfile.avatar : baseUserImage
                  }
                  alt="User profile image"
                />
              </div>

              <div className="user-comparison__filter">
                <Form
                  onSubmit={(option) => {
                    if (option) {
                      setProfileLastSelectedId(option.player_name.value);
                    }
                  }}
                  render={({ handleSubmit }) => (
                    <form className="user-profile-form" onSubmit={handleSubmit}>
                      <div className="user-profile-form__input-group">
                        <Field
                          name="player_name"
                          render={({ input }) => (
                            <>
                              <AsyncSelect
                                className="input-select-container"
                                styles={customStylesInputSelect}
                                name={input.name}
                                placeholder="Enter player name"
                                onChange={(option) => {
                                  input.onChange(option);

                                  handleSubmit();
                                }}
                                cacheOptions
                                defaultOptions
                                loadOptions={handleGetProfilesName}
                              />
                              <span className="user-profile-form__filter-icon">
                                <img
                                  width={16}
                                  height={16}
                                  src={iconSearch}
                                  alt="Icon Search"
                                />
                              </span>
                            </>
                          )}
                        />
                      </div>
                    </form>
                  )}
                />
              </div>
            </div>

            <div className="user-comparison__user-info">
              <table className="user-data-table">
                <tbody>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Age:</td>
                    <td className="user-data-table__coll">
                      {lastProfile?.age || '-'}
                    </td>
                  </tr>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Height:</td>
                    <td className="user-data-table__coll">
                      {lastProfile?.feet
                        ? `${lastProfile.feet} ft ${
                            lastProfile.inches
                              ? `${lastProfile.inches} in`
                              : '0 in'
                          }`
                        : '-'}
                    </td>
                  </tr>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Weight:</td>
                    <td className="user-data-table__coll">
                      {lastProfile?.weight ? `${lastProfile?.weight} lbs` : '-'}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </>
      )}

      {!!firstProfileId === false && (
        <>
          <div className="comparison__user-container user-comparison">
            <div className="user-comparison__header">
              <div className="user-comparison__image">
                <img
                  width={40}
                  height={40}
                  src={
                    firstProfile?.avatar ? firstProfile.avatar : baseUserImage
                  }
                  alt="User profile image"
                />
              </div>

              <div className="user-comparison__filter">
                <Form
                  onSubmit={(values) =>
                    setProfileFirstSelectedId(values.player)
                  }
                  render={({ handleSubmit }) => (
                    <form
                      className="comparison-filters"
                      onSubmit={handleSubmit}>
                      <Field
                        name="player"
                        type="text"
                        parse={(option) =>
                          !option.value ? undefined : option.value
                        }
                        render={({ input }) => (
                          <Select
                            styles={customStylesSelect}
                            options={players.map((player) => {
                              return {
                                value: player.id,
                                label: `${player.first_name} ${player.last_name}`,
                              };
                            })}
                            placeholder="Select Profile"
                            onChange={(option) => {
                              input.onChange(option);
                              handleSubmit();
                            }}
                            isSearchable={false}
                          />
                        )}
                      />
                    </form>
                  )}
                />
              </div>
            </div>

            <div className="user-comparison__user-info">
              <table className="user-data-table">
                <tbody>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Age:</td>
                    <td className="user-data-table__coll">
                      {firstProfile?.age || '-'}
                    </td>
                  </tr>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Height:</td>
                    <td className="user-data-table__coll">
                      {firstProfile?.feet
                        ? `${firstProfile.feet} ft ${
                            firstProfile.inches
                              ? `${firstProfile.inches} in`
                              : '0 in'
                          }`
                        : '-'}
                    </td>
                  </tr>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Weight:</td>
                    <td className="user-data-table__coll">
                      {firstProfile?.weight
                        ? `${firstProfile?.weight} lbs`
                        : '-'}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className="comparison__user-container user-comparison">
            <div className="user-comparison__header">
              <div className="user-comparison__image">
                <img
                  width={40}
                  height={40}
                  src={
                    lastProfile?.avatar ? lastProfile?.avatar : baseUserImage
                  }
                  alt="User profile image"
                />
              </div>

              <div className="user-comparison__filter">
                <Form
                  onSubmit={(values) => setProfileLastSelectedId(values.player)}
                  render={({ handleSubmit }) => (
                    <form
                      className="comparison-filters"
                      onSubmit={handleSubmit}>
                      <Field
                        name="player"
                        type="text"
                        parse={(option) =>
                          !option.value ? undefined : option.value
                        }
                        render={({ input }) => (
                          <Select
                            styles={customStylesSelect}
                            options={players.map((player) => {
                              return {
                                value: player.id,
                                label: `${player.first_name} ${player.last_name}`,
                              };
                            })}
                            placeholder="Select Profile"
                            onChange={(option) => {
                              input.onChange(option);
                              handleSubmit();
                            }}
                            isSearchable={false}
                          />
                        )}
                      />
                    </form>
                  )}
                />
              </div>
            </div>

            <div className="user-comparison__user-info">
              <table className="user-data-table">
                <tbody>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Age:</td>
                    <td className="user-data-table__coll">
                      {lastProfile?.age || '-'}
                    </td>
                  </tr>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Height:</td>
                    <td className="user-data-table__coll">
                      {lastProfile?.feet
                        ? `${lastProfile.feet} ft ${
                            lastProfile.inches
                              ? `${lastProfile.inches} in`
                              : '0 in'
                          }`
                        : '-'}
                    </td>
                  </tr>
                  <tr className="user-data-table__row">
                    <td className="user-data-table__coll">Weight:</td>
                    <td className="user-data-table__coll">
                      {lastProfile?.weight ? `${lastProfile?.weight} lbs` : '-'}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </>
      )}

      <ComparisonTable
        firstProfile={firstProfile}
        lastProfile={lastProfile}
        type={isPitcher ? 'Pitching' : 'Batting'}
      />
    </section>
  );
};

Comparison.defaultProps = {
  firstProfileId: '',
  players: [],
} as Partial<ComparisonProps>;

export default Comparison;
