import { FORM_ERROR } from 'final-form';
import React from 'react';
import { Form, Field } from 'react-final-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { AppDispatch } from 'src/redux/store';
import { userSelectors, userOperations } from 'src/redux/user';
import { UserSignUpRequest } from 'src/types';
import { required } from 'src/utils/validate';
import FieldButton from '../FieldButton';
import FormContainer from '../FormContainer';
import IconCheck from '../Icons/IconCheck';
import IconPassword from '../Icons/IconPassword';
import IconUser from '../Icons/IconUser';
import Input from '../Input';
import './styles.scss';

const FormRegistration = (): JSX.Element => {
  const dispatch: AppDispatch = useDispatch();
  const loading = useSelector(userSelectors.selectStatusLoading);

  const handleSignUp = async (userData: UserSignUpRequest) => {
    const resultAction = await dispatch(userOperations.fetchSignUp(userData));
    if (!userOperations.fetchSignUp.fulfilled.match(resultAction)) {
      if (resultAction.payload) {
        debugger
        return { email: resultAction.payload.errors.email[0],
        };
      }
    }
  };

  return (
    <FormContainer classModification="form-container--registration">
      <Form
        initialValues={{ role: 'player' }}
        onSubmit={handleSignUp}
        render={({ handleSubmit, values, submitError }) => (
          <form className="form-registration" onSubmit={handleSubmit}>
            <div className="form-registration__role role">
              <div className="role__input-group">
                <Field
                  name="role"
                  type="radio"
                  value="player"
                  render={({ input }) => (
                    <>
                      <input
                        {...input}
                        className="visually-hidden"
                        id="radio-player"
                      />
                      <label htmlFor="radio-player" className="role__label">
                        Sign Up as Player
                      </label>
                    </>
                  )}
                />
                <Field
                  name="role"
                  type="radio"
                  value="scout"
                  render={({ input }) => (
                    <>
                      <input
                        {...input}
                        className="visually-hidden"
                        id="radio-scout"
                      />
                      <label htmlFor="radio-scout" className="role__label">
                        Sign Up as Scout
                      </label>
                    </>
                  )}
                />
              </div>

              <div className="role__description">
                <p className="role__description-title">
                  {values.role === 'player' ? 'Players' : 'Scouts'}
                </p>
                <p className="role__description-text">
                  {values.role === 'player'
                    ? 'Players have their own profile within the system and plan on having data collected.'
                    : 'Coaches and scouts can view players in the system but do not have their own profile.'}
                </p>
              </div>
            </div>

            <Field
              name="email"
              placeholder="Email"
              type="email"
              validate={required}
              render={({ input, meta, placeholder }) => (
                <p className="form-registration__input-group">
                  <Input placeholder={placeholder} {...input}>
                    <IconUser />
                  </Input>
                  {(meta.error || meta.submitError) && meta.touched && (
                    <span className="form-registration__text-error">
                      {meta.error || meta.submitError}
                    </span>
                  )}
                </p>
              )}
            />

            <Field
              name="password"
              placeholder="Password"
              type="password"
              validate={required}
              render={({ input, meta, placeholder }) => (
                <p className="form-registration__input-group">
                  <Input placeholder={placeholder} {...input}>
                    <IconPassword />
                  </Input>
                  {(meta.error || meta.submitError) && meta.touched && (
                    <span className="form-registration__text-error">
                      {meta.error || meta.submitError}
                    </span>
                  )}
                </p>
              )}
            />

            <Field
              name="password_confirmation"
              placeholder="Confirm Password"
              type="password"
              validate={required}
              render={({ input, meta, placeholder }) => (
                <p className="form-registration__input-group">
                  <Input placeholder={placeholder} {...input}>
                    <IconCheck />
                  </Input>
                  {(meta.error || meta.submitError) && meta.touched && (
                    <span className="form-registration__text-error">
                      {meta.error || meta.submitError}
                    </span>
                  )}
                </p>
              )}
            />
            <span className="form-registration__text-error">{submitError}</span>
            <FieldButton title="Sign Up" disabled={loading === 'pending'} />
          </form>
        )}
      />

      <p className="form-container__info-text">
        By clicking Sign Up, you agree to our
        <Link to="/legal/terms"> Terms of Service </Link> and
        <Link to="/legal/privacy"> Privacy Policy </Link>
      </p>

      <div className="form-container__sign-in sign-in">
        <span className="sing-in__text">Already registered? </span>
        <Link to="/login" className="sign-in__link">
          Sign In
        </Link>
      </div>
    </FormContainer>
  );
};

export default FormRegistration;
