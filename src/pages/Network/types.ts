import { School, Teams } from 'src/types';

export interface ProfilesNetwork {
  age: number;
  events: [];
  favorite: boolean;
  feet: number;
  first_name: string;
  id: string;
  inches: number;
  last_name: string;
  position: string;
  position2: string;
  school: School;
  school_year: number;
  teams: Teams;
  weight: number;
}

export interface ProfilesRequest {
  data: {
    profiles: {
      profiles: ProfilesNetwork[];
      total_count: number;
    };
  };
}

export interface FilterNetworkType {
  age?: number;
  favorite?: number;
  offset: number;
  player_name?: string;
  position?: string;
  profiles_count: number;
  school?: string;
  team?: string;
}
