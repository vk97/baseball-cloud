import React from 'react';
import MainContainer from '../../components/MainContainer';
import FormRegistration from '../../components/FormRegistration';
import { MainLayoutModification } from 'src/types';

const SignUp = (): JSX.Element => {
  return (
    <MainContainer styleModification={MainLayoutModification.AUTHORIZATION}>
      <FormRegistration />
    </MainContainer>
  );
};

export default SignUp;
