import React from 'react';
import './styles.scss';

interface FormContainerProps {
  children: React.ReactNode;
  classModification?: 'form-container--registration' | string;
}

const FormContainer = ({
  children,
  classModification,
}: FormContainerProps): JSX.Element => {
  return (
    <div className={`form-container ${classModification}`}>{children}</div>
  );
};

FormContainer.defaultProps = {
  classModification: '',
} as Partial<FormContainerProps>;

export default FormContainer;
