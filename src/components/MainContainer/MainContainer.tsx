import React from 'react';
import { MainLayoutModification } from 'src/types';
import Footer from '../Footer';
import Header from '../Header';
import SiteScreen from '../SiteScreen';
import './styles.scss';

interface MainContainerProps {
  children: React.ReactNode;
  styleModification: MainLayoutModification;
}

const MainContainer = ({
  children,
  styleModification,
}: MainContainerProps): JSX.Element => {
  let classAuthorization = '';

  if (styleModification !== MainLayoutModification.BASE) {
    classAuthorization = 'page-main--authorization';
  }

  return (
    <SiteScreen>
      <Header />
      <main className={`page-main ${classAuthorization}`}>{children}</main>
      <Footer />
    </SiteScreen>
  );
};

MainContainer.defaultProps = {
  styleModification: MainLayoutModification.BASE,
} as Partial<MainContainerProps>;

export default MainContainer;
