import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { userSelectors } from 'src/redux/user';

interface PrivateRouteProps {
  render: (props: any) => React.ReactNode;
  path: string | string[];
  exact: boolean;
}

const PrivateRoute = ({
  render,
  exact,
  path,
}: PrivateRouteProps): JSX.Element => {
  const dispatch = useDispatch();
  const userData = useSelector(userSelectors.selectUserData);
  const userStatus = useSelector(userSelectors.selectStatusLoading);
  const token = useSelector(userSelectors.selectToken);

  return (
    <Route
      exact={exact}
      path={path}
      render={(props) => {
        return token !== null && userData !== null ? (
          render(props)
        ) : (
          <Redirect to="/login" />
        );
      }}
    />
  );
};

export default PrivateRoute;
