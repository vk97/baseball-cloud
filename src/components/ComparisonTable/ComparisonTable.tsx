import React, { useEffect, useState } from 'react';
import { Form, Field } from 'react-final-form';
import Select from 'react-select';
import { PlayerProfile } from 'src/redux/data/types';
import { customStylesSelect } from 'src/styles/customStyles';
import './styles.scss';

interface ComparisonTableProps {
  firstProfile: PlayerProfile | undefined;
  lastProfile: PlayerProfile | undefined;
  type?: 'Batting' | 'Pitching';
}

type BattingVariantValuesType =
  | 'distance'
  | 'exit_velocity'
  | 'launch_angle'
  | 'spin_rate'
  | 'velocity';

interface TopBattingValuesProfile {
  distance: number;
  exit_velocity: number;
  launch_angle: number;
  pitch_type: string;
}
interface TopPitchingValuesProfile {
  pitch_type: string;
  spin_rate: number;
  velocity: number;
}

const ComparisonTable = ({
  firstProfile,
  lastProfile,
  type,
}: ComparisonTableProps): JSX.Element => {
  const [activeType, setActiveType] = useState<BattingVariantValuesType>(
    'exit_velocity',
  );
  const optionsBatting = [
    { value: 'exit_velocity', label: 'Exit Velocity' },
    { value: 'distance', label: 'Distance' },
    { value: 'launch_angle', label: 'Launch Angle' },
  ];

  const optionsPitching = [
    { value: 'velocity', label: 'Velocity' },
    { value: 'spin_rate', label: 'Spin Rate' },
  ];

  useEffect(() => {
    if (type === 'Batting') {
      setActiveType('exit_velocity');
    } else {
      setActiveType('velocity');
    }
  }, [type]);

  const getFastBallValues = (
    playerProfile: PlayerProfile | null,
    pitchType: string,
  ) => {
    if (playerProfile) {
      if (type === 'Batting') {
        const playerValues:
          | TopBattingValuesProfile
          | undefined = playerProfile.batting_top_values.find(
          (values: TopBattingValuesProfile) => values.pitch_type === pitchType,
        );

        if (playerValues) {
          return playerValues[activeType];
        } else {
          return '-';
        }
      } else {
        const playerValues:
          | TopPitchingValuesProfile
          | undefined = playerProfile.pitching_top_values.find(
          (values: TopPitchingValuesProfile) => values.pitch_type === pitchType,
        );

        if (playerValues) {
          return playerValues[activeType];
        } else {
          return '-';
        }
      }
    }

    return '-';
  };

  return (
    <section className="comparison__table-container table-comparison">
      <div className="table-comparison__filter">
        <Form
          onSubmit={(values) => setActiveType(values.pitch_type)}
          render={({ handleSubmit }) => (
            <form className="comparison-filters" onSubmit={handleSubmit}>
              <Field
                name="pitch_type"
                type="text"
                parse={(option) => (!option.value ? undefined : option.value)}
                render={({ input }) => (
                  <Select
                    styles={customStylesSelect}
                    options={
                      type === 'Batting' ? optionsBatting : optionsPitching
                    }
                    placeholder={`Top ${type} Values`}
                    onChange={(option) => {
                      input.onChange(option);
                      handleSubmit();
                    }}
                    isSearchable={false}
                  />
                )}
              />
            </form>
          )}
        />
      </div>

      <table className="comparison__table table-data">
        <tbody>
          <tr className="table-data__row">
            <td className="table-data__coll">Fastball</td>
            <td className="table-data__coll">
              {firstProfile && getFastBallValues(firstProfile, 'Fastball')}
            </td>
            <td className="table-data__coll">
              {lastProfile && getFastBallValues(lastProfile, 'Fastball')}
            </td>
          </tr>
          <tr className="table-data__row">
            <td className="table-data__coll">Curveball</td>
            <td className="table-data__coll">
              {firstProfile && getFastBallValues(firstProfile, 'Curveball')}
            </td>
            <td className="table-data__coll">
              {lastProfile && getFastBallValues(lastProfile, 'Curveball')}
            </td>
          </tr>
          <tr className="table-data__row">
            <td className="table-data__coll">Changeup</td>
            <td className="table-data__coll">
              {firstProfile && getFastBallValues(firstProfile, 'Changeup')}
            </td>
            <td className="table-data__coll">
              {lastProfile && getFastBallValues(lastProfile, 'Changeup')}
            </td>
          </tr>
          <tr className="table-data__row">
            <td className="table-data__coll">Slider</td>
            <td className="table-data__coll">
              {firstProfile && getFastBallValues(firstProfile, 'Slider')}
            </td>
            <td className="table-data__coll">
              {lastProfile && getFastBallValues(lastProfile, 'Slider')}
            </td>
          </tr>
        </tbody>
      </table>
    </section>
  );
};

ComparisonTable.defaultProps = {
  type: 'Batting',
} as Partial<ComparisonTableProps>;

export default ComparisonTable;
