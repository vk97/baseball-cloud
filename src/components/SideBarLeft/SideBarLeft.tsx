import React from 'react';
import './styles.scss';

interface SideBarLeftProps {
  children: React.ReactNode;
}

const SideBarLeft = ({ children }: SideBarLeftProps): JSX.Element => {
  return <aside className="side-bar-left">{children}</aside>;
};

export default SideBarLeft;
