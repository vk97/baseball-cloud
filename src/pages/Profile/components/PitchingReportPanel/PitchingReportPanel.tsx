import React from 'react';
import LoaderData from 'src/components/LoaderData';
import { PlayerProfile } from 'src/redux/data/types';
import { UserProfile } from 'src/redux/user/types';
import { PitchingSummary } from 'src/types';
import BattingAndPitchingSummary from '../BattingAndPitchingSummary';
import ChartVelocity from '../ChartVelocity';
import LogTable from '../LogTable';
import { VariantBattingAndPitchingType } from '../UserProfileReport/types';

interface PitchingReportPanelProps {
  userProfile: PlayerProfile;
  variant: VariantBattingAndPitchingType;
  pitchingSummary: PitchingSummary;
}

const PitchingReportPanel = ({
  userProfile,
  variant,
  pitchingSummary,
}: PitchingReportPanelProps): JSX.Element => {
  const columnsTableSummary = [
    { Header: 'Pitch Type', accessor: 'pitch_type' },
    { Header: 'Velocity', accessor: 'velocity' },
    { Header: 'Spin Rate', accessor: 'spin_rate' },
  ];
  const columnsTableLog = [
    { Header: 'Date', accessor: 'date' },
    { Header: 'Batter Name', accessor: 'batter_name' },
    { Header: 'Pitch Type', accessor: 'pitch_type' },
    { Header: 'Pitch Call', accessor: 'pitch_call' },
    { Header: 'Velocity', accessor: 'velocity' },
    { Header: 'Spin Rate', accessor: 'spin_rate' },
    { Header: 'Spin Axis', accessor: 'spin_axis' },
  ];

  if (userProfile?.id) {
    switch (variant) {
      case 'Summary':
        return (
          <BattingAndPitchingSummary
            type="Pithing"
            dataSummary={pitchingSummary}
            columns={columnsTableSummary}
          />
        );
      case 'Charts':
        return (
          <ChartVelocity
            variantChart="Pithing"
            profileName={`${userProfile?.first_name} ${userProfile?.last_name}`}
            profileId={userProfile.id}
          />
        );
      case 'Log':
        return (
          <LogTable
            type="Pithing"
            columns={columnsTableLog}
            profileId={userProfile.id}
          />
        );
      default:
        return (
          <BattingAndPitchingSummary
            type="Pithing"
            dataSummary={pitchingSummary}
            columns={columnsTableSummary}
          />
        );
    }
  } else {
    return <LoaderData />;
  }
};

export default PitchingReportPanel;
