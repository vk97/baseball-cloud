import React, { useEffect, useMemo, useState } from 'react';
import { useAsync } from 'react-async-hook';
import Card from 'src/components/Card';
import EmptyData from 'src/components/EmptyData';
import Table from 'src/components/Table';
import { VelocityChartRows } from 'src/types';
import FetchData from 'src/utils/FetchData';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import './styles.scss';
import LoaderData from 'src/components/LoaderData';

interface SessionHittingProps {
  idEvent: string;
  idProfile: string;
}

const SessionPitching = ({
  idEvent,
  idProfile,
}: SessionHittingProps): JSX.Element => {
  const [chartRows, setChartRows] = useState<VelocityChartRows[] | []>([]);
  const asyncSessionPitching = useAsync(
    FetchData.fetchEventPitchingSummary,
    [idEvent, idProfile],
    {
      onSuccess(result) {
        setChartRows(
          result.data.data.event_pitching_summary.velocity_chart_rows,
        );
        return result;
      },
    },
  );

  useEffect(() => {
    return () => {
      asyncSessionPitching.reset();
    };
  }, []);

  const dataTable = useMemo(() => {
    return (
      asyncSessionPitching.result?.data.data.event_pitching_summary
        .pitching_summary_rows || []
    );
  }, [asyncSessionPitching.result]);

  const dataCharts = (chartRows as VelocityChartRows[]).map((velocity) =>
    Number(velocity.velo),
  );

  const columnTable = useMemo(
    () => [
      { Header: 'Pitcher Name', accessor: 'pitcher_name' },
      { Header: 'Pitcher Team', accessor: 'pitcher_team' },
      { Header: 'Pitch Count', accessor: 'pitch_count' },
      { Header: 'Pitch Type', accessor: 'pitch_type' },
      { Header: 'Ball Count', accessor: 'ball_count' },
      { Header: 'Called Strike Count', accessor: 'called_strike_count' },
      { Header: 'Swing Strike Count', accessor: 'swing_strike_count' },
      { Header: 'Avg Velo', accessor: 'avg_velo' },
      { Header: 'Max Velo', accessor: 'max_velo' },
      { Header: 'Avg Spin', accessor: 'avg_spin' },
      { Header: 'Max Spin', accessor: 'max_spin' },
    ],
    [],
  );
  const pitcherChartName = chartRows.length > 0 && chartRows[0].pitcher_name;

  const options: Highcharts.Options = {
    title: {
      text: 'Exit Velocity',
    },
    tooltip: {
      pointFormat: `${pitcherChartName} <b>{point.y:.1f}</b>`,
    },
    series: [
      {
        type: 'line',
        name: `${pitcherChartName}`,
        data: dataCharts,
      },
    ],
  };

  return (
    <Card
      title="Data collected for all pitchers who participated in this session."
      classElement="session-pitching">
      <div className="session-pitching__chart"></div>
      <div className="session-pitching__table">
        {asyncSessionPitching.loading && <LoaderData />}

        {asyncSessionPitching.result && dataTable.length > 0 && (
          <>
            <HighchartsReact highcharts={Highcharts} options={options} />
            <Table columns={columnTable} data={dataTable} />
          </>
        )}
        {dataTable.length === 0 && !asyncSessionPitching.loading && (
          <EmptyData />
        )}
      </div>
    </Card>
  );
};

export default SessionPitching;
