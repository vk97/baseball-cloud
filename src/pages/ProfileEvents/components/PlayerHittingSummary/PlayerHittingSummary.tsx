import React, { useMemo } from 'react';
import { useAsync } from 'react-async-hook';
import Card from 'src/components/Card';
import LoaderData from 'src/components/LoaderData';
import Table from 'src/components/Table';
import FetchData from 'src/utils/FetchData';
import './styles.scss';

interface PlayerHittingSummaryProps {
  idProfile: string;
  idEvent: string;
}

const PlayerHittingSummary = ({
  idProfile,
  idEvent,
}: PlayerHittingSummaryProps): JSX.Element => {
  const asyncPlayerHittingSummary = useAsync(
    FetchData.fetchEventPlayerSummary,
    [idEvent, idProfile],
  );

  const dataTable = useMemo(() => {
    return (
      asyncPlayerHittingSummary.result?.data.data.event_player_summary
        .hitting_summary_table_rows || []
    );
  }, [asyncPlayerHittingSummary.result]);
  const columnTable = useMemo(
    () => [
      { Header: 'Pitch Type', accessor: 'pitch_type' },
      { Header: 'Exit Velocity', accessor: 'exit_velocity' },
      { Header: 'Launch Angle', accessor: 'launch_angle' },
      { Header: 'Distance', accessor: 'distance' },
      { Header: 'Pitch Call', accessor: 'pitch_call' },
    ],
    [],
  );

  return (
    <Card title="Player Summary" classElement="player-summary">
      {asyncPlayerHittingSummary.result?.data.data.event_player_summary
        .hitting_summary_table_rows.length === 0 ? (
        <p className="player-summary__text">There s no info yet!</p>
      ) : (
        <>
          {asyncPlayerHittingSummary.loading && <LoaderData />}
          {asyncPlayerHittingSummary.result && (
            <>
              <p className="player-summary__info-table">
                This table shows your hitting performance by pitch type in the
                session.
              </p>
              <Table data={dataTable} columns={columnTable} />
            </>
          )}
        </>
      )}
    </Card>
  );
};

export default PlayerHittingSummary;
