import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import MainContainer from 'src/components/MainContainer';
import { TypeAccount } from 'src/types';
import { customHistory } from 'src/App';
import { dataOperations } from 'src/redux/data';
import { RouteComponentProps } from 'react-router-dom';
import LoaderData from 'src/components/LoaderData';
import { dataSelectors } from 'src/redux/data';
import { userOperations } from 'src/redux/user';
import { playerProfileSelectors, playerProfileOperations } from 'src/redux/playerProfile';
import { PlayerProfile, CurrentProfile } from './components';
import { userSelectors } from 'src/redux/user';
import './styles.scss';

type RouteParams = {
  id?: string;
};

const Profile = ({ match }: RouteComponentProps<RouteParams>): JSX.Element => {
  const dispatch = useDispatch();
  const userData = useSelector(userSelectors.selectUserData);
  const userProfile = useSelector(playerProfileSelectors.selectPlayerProfile);
  const currentProfile = useSelector(userSelectors.selectCurrentProfile);
  const loadingProfile = useSelector(
    playerProfileSelectors.selectLoadingPlayerProfile,
  );
  const loadingCurrentProfile = useSelector(
    userSelectors.selectStatusLoadingCurrentProfile,
  );
  const errorLoadingProfile = useSelector(dataSelectors.selectErrorData);
  const isCurrentProfile = !!!match.params.id;

  if (userData?.role === TypeAccount.SCOUT && !!match.params.id === false) {
    customHistory.push('/dashboard');
  }

  useEffect(() => {
    if (isCurrentProfile) {
      dispatch(userOperations.fetchCurrentProfile(null));
      dispatch(dataOperations.fetchTeams(null));
      dispatch(dataOperations.fetchSchools(null));
      dispatch(dataOperations.fetchFacilities(null));
    } else if (!!match.params.id) {
      dispatch(playerProfileOperations.fetchPlayerProfile(match.params.id));
    }
  }, [match.params.id]);

  const isLoading =
    loadingProfile === 'pending' || loadingCurrentProfile === 'pending';

  return (
    <MainContainer>
      <section className="user-profile">
        {isLoading && (
          <div className="user-profile__loading">
            <LoaderData />
          </div>
        )}
        {errorLoadingProfile !== undefined && !isLoading && (
          <p className="user-profile__error-loading">{errorLoadingProfile}</p>
        )}
        {isCurrentProfile &&
          !isLoading &&
          errorLoadingProfile === undefined &&
          currentProfile !== null && (
            <CurrentProfile userProfile={currentProfile} />
          )}

        {!isCurrentProfile &&
          !isLoading &&
          errorLoadingProfile === undefined &&
          userProfile !== null && <PlayerProfile userProfile={userProfile} />}
      </section>
    </MainContainer>
  );
};

export default Profile;
