import React from 'react';
import { Field, Form } from 'react-final-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { userSelectors, userOperations } from 'src/redux/user';
import { required } from 'src/utils/validate';
import FieldButton from '../FieldButton';
import FormContainer from '../FormContainer';
import IconUser from '../Icons/IconUser';
import Input from '../Input';

const FormForgotPassword = (): JSX.Element => {
  const dispatch = useDispatch();
  const loading = useSelector(userSelectors.selectStatusLoading);
  const errorRequest = useSelector(userSelectors.selectErrorForgotPassword);

  return (
    <FormContainer>
      <div className="form-container__header">
        <p className="form-container__title">Forgot Password</p>
        <p className="form-container__subtitle">
          Please enter your email address. You will receive a link to reset your
          password via email.
        </p>
      </div>

      <Form
        onSubmit={({ email }) => dispatch(userOperations.fetchForgotPassword(email))}
        render={({ handleSubmit }) => (
          <form className="form-auth" onSubmit={handleSubmit}>
            <Field
              name="email"
              placeholder="Email"
              type="email"
              validate={required}
              render={({ input, meta, placeholder }) => (
                <p className="form-auth__input-group">
                  <Input placeholder={placeholder} {...input}>
                    <IconUser />
                  </Input>
                  {meta.touched && meta.error && (
                    <span className="form-auth__text-error">{meta.error}</span>
                  )}
                </p>
              )}
            />
            <span className="form-auth__text-error">{errorRequest}</span>
            <FieldButton title="Submit" disabled={loading === 'pending'} />
          </form>
        )}
      />

      <div className="form-container__sign-in sign-in">
        <span className="sing-in__text">Remember password? </span>
        <Link to="/login" className="sign-in__link">
          Sign In
        </Link>
      </div>
    </FormContainer>
  );
};

export default FormForgotPassword;
