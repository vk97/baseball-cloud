import React, { useEffect, useState } from 'react';
import { useAsync, useAsyncCallback } from 'react-async-hook';
import { useSelector } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import Select from 'react-select';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';
import { customHistory } from 'src/App';
import IconLock from 'src/components/Icons/IconLock';
import LoaderData from 'src/components/LoaderData';
import MainContainer from 'src/components/MainContainer';
import { userSelectors } from 'src/redux/user';
import { customStylesSelect } from 'src/styles/customStyles';
import { ProfilesInEvent, RecentEventType } from 'src/types';
import FetchData from 'src/utils/FetchData';
import {
  PlayerHittingSummary,
  SprayChart,
  HittingStrikeZoneChart,
  PlayerLog,
  SessionHitting,
  SessionPitching,
  LockScreen,
} from './components';
import './styles.scss';

type RouteParams = {
  idProfile: string;
  idEvent: string;
};

const ProfileEvents = ({
  match,
}: RouteComponentProps<RouteParams>): JSX.Element => {
  const [idProfile, setIdProfile] = useState<string>(match.params.idProfile);
  const [idEvent, setIdEvent] = useState<string>(match.params.idEvent);
  const [profilesInEvent, setProfilesInEvent] = useState<
    ProfilesInEvent[] | null
  >(null);
  const [activeProfile, setActiveProfile] = useState<ProfilesInEvent | null>(
    null,
  );
  const [] = useState<RecentEventType[] | null>(null);
  const [activeEvent, setActiveEvent] = useState<RecentEventType | null>(null);
  const userData = useSelector(userSelectors.selectUserData);

  userData?.role === 'player' && customHistory.push('/');

  const handleGetProfilesInEvent = useAsyncCallback(
    () => {
      return FetchData.fetchProfilesInEvent(idEvent);
    },
    {
      onSuccess(result) {
        if (result.data.data) {
          const activeProfile = result.data.data.profiles.profiles.find(
            (profile) => profile.id === idProfile,
          );
          setProfilesInEvent(result.data.data.profiles.profiles);
          activeProfile && setActiveProfile(activeProfile);
        }
      },
    },
  );

  const asyncEvents = useAsync(FetchData.fetchEvents, [idProfile], {
    onSuccess(result) {
      const activeEvent = result.data.data.profile_events.events.find(
        (event) => event.id === idEvent,
      );
      activeEvent && setActiveEvent(activeEvent);
    },
  });

  const asyncEventDetail = useAsync(FetchData.fetchEventDetail, [
    idEvent,
    idProfile,
  ]);

  useEffect(() => {
    if (!handleGetProfilesInEvent.loading) {
      handleGetProfilesInEvent.execute();
    }

    return () => {
      handleGetProfilesInEvent.reset();
    };
  }, [idEvent]);

  return (
    <MainContainer>
      <section className="profile-events">
        <div className="profile-events__header">
          {handleGetProfilesInEvent.loading && <LoaderData />}
          {profilesInEvent &&
            activeProfile &&
            !handleGetProfilesInEvent.loading && (
              <Select
                styles={customStylesSelect}
                isSearchable={false}
                value={{
                  value: activeProfile.id,
                  label: `${activeProfile.first_name} ${activeProfile.last_name}`,
                }}
                options={profilesInEvent.map((profile) => {
                  return {
                    value: profile.id,
                    label: `${profile.first_name} ${profile.last_name}`,
                  };
                })}
                onChange={(option) => {
                  const activeProfile = profilesInEvent.find(
                    (profile) => profile.id === option?.value,
                  );

                  if (activeProfile) {
                    setActiveProfile(activeProfile);
                    customHistory.push(
                      `/profile/${activeProfile.id}/event/${idEvent}`,
                    );
                    setIdProfile(activeProfile.id);
                  }

                  return option;
                }}
              />
            )}
        </div>
        <div className="profile-events__data">
          <aside className="side-bar">
            <ul className="list-view">
              {asyncEvents.loading && <LoaderData />}
              {asyncEvents.result &&
                (asyncEvents.result.data.data.profile_events
                  .events as RecentEventType[]).map((event) => {
                  let lockElement;
                  let activeClass;

                  if (event.event_type === 'Game') {
                    lockElement =
                      event.id === match.params.idEvent ? (
                        <span className="status">
                          <IconLock color="#ffffff" />
                        </span>
                      ) : (
                        <span className="status">
                          <IconLock />
                        </span>
                      );
                  }

                  if (event.id === match.params.idEvent) {
                    activeClass = 'list-view__link--active';
                  }

                  return (
                    <li
                      className="list-view__item"
                      key={event.event_name + event.id}>
                      <Link
                        to={`/profile/${match.params.idProfile}/event/${event.id}`}
                        className={`list-view__link ${activeClass}`}
                        onClick={() => {
                          setActiveEvent(event);
                          setIdEvent(event.id);
                        }}>
                        <span className="list-view__date">{event.date}</span>
                        <span className="list-view__type-event">
                          {event.event_type}
                        </span>
                        <span className="list-view__name">
                          {event.event_name}
                        </span>
                        {lockElement}
                      </Link>
                    </li>
                  );
                })}
            </ul>
          </aside>
          {(asyncEvents.loading || asyncEventDetail.loading) && <LoaderData />}
          {!asyncEvents.loading &&
            !asyncEventDetail.loading &&
            (activeEvent?.event_type !== 'Game' ? (
              <Tabs className="event-tabs">
                <TabList className="event-tabs__tab-list">
                  <Tab
                    className="event-tabs__tab-event"
                    selectedClassName="event-tabs__tab-event--selected">
                    Player Summary
                  </Tab>
                  <Tab
                    className="event-tabs__tab-event"
                    selectedClassName="event-tabs__tab-event--selected">
                    Player Log
                  </Tab>
                  <Tab
                    className="event-tabs__tab-event"
                    selectedClassName="event-tabs__tab-event--selected">
                    Session Hitting
                  </Tab>
                  <Tab
                    className="event-tabs__tab-event"
                    selectedClassName="event-tabs__tab-event--selected">
                    Session Pitching
                  </Tab>
                </TabList>

                <TabPanel
                  className="event-tabs__content"
                  selectedClassName="event-tabs__content--selected">
                  <PlayerHittingSummary
                    idProfile={idProfile}
                    idEvent={idEvent}
                  />
                  <div className="event-tabs__content-chart">
                    <SprayChart />
                    <HittingStrikeZoneChart />
                  </div>
                </TabPanel>
                <TabPanel
                  className="event-tabs__content"
                  selectedClassName="event-tabs__content--selected">
                  <PlayerLog idEvent={idEvent} idProfile={idProfile} />
                </TabPanel>
                <TabPanel
                  className="event-tabs__content"
                  selectedClassName="event-tabs__content--selected">
                  <SessionHitting idEvent={idEvent} idProfile={idProfile} />
                </TabPanel>
                <TabPanel
                  className="event-tabs__content"
                  selectedClassName="event-tabs__content--selected">
                  <SessionPitching idEvent={idEvent} idProfile={idProfile} />
                </TabPanel>
              </Tabs>
            ) : (
              <>
                {asyncEventDetail.result && (
                  <LockScreen
                    eventDetail={asyncEventDetail.result.data.data.event_detail}
                  />
                )}
              </>
            ))}
        </div>
      </section>
    </MainContainer>
  );
};

export default ProfileEvents;
