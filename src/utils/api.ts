import axios, { AxiosInstance } from 'axios';
import axiosRetry from 'axios-retry';
import { toast } from 'react-toastify';
import { RootState } from 'src/redux/reducer';
import { store } from 'src/redux/store';

toast.configure();

enum ErrorCode {
  UNAUTHORIZED = 401,
  BADREQUEST = 400, 
}

const createAPI = (): AxiosInstance => {
  const api = axios.create({
    baseURL: `https://baseballcloud-back.herokuapp.com`,
    timeout: 20000,
  });

  axiosRetry(axios, {
    retries: 2,
    retryDelay: (retryCount) => {
      return retryCount * 1000;
    },
    retryCondition: axiosRetry.isRetryableError,
  });

  api.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      const { response } = error;
      if (error.response) {
        switch (response.status) {
          case ErrorCode.UNAUTHORIZED:
            throw new Error('Invalid login credentials. Please try again.');
            break;
          case ErrorCode.BADREQUEST:
            throw new Error('Failed to complete the request');
            break;
        }

       return response;
      } else if (error.request) {
        toast.error(`Request failed. Check your internet connection`, {
          position: `top-right`,
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });

        throw error;
      }

      throw error;
    },
  );

  const state: RootState = store.getState();
  const { token, client } = state.USER;
  let email = '';
  if (state.USER.userData) {
    email = state.USER.userData.email;
  }

  api.interceptors.request.use(function (config) {
    config.headers['access-token'] = token ? token : '';
    config.headers.uid = email ? email : '';
    config.headers.client = client ? client : '';
    return config;
  });

  return api;
};

export default createAPI;
