import { LoadingType } from 'src/types';
import { PlayerProfile } from '../data/types';
import { RootState } from '../reducer';

const selectLoadingPlayerProfile = (state: RootState): LoadingType =>
  state.PLAYER.loadingPlayerProfile;
const selectPlayerProfile = (state: RootState): PlayerProfile | null =>
  state.PLAYER.playerProfile;

export default {
  selectLoadingPlayerProfile,
  selectPlayerProfile,
}
