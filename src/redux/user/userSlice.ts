import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { LoadingType } from 'src/types';
import { userOperations } from './index';
import { UserData, UserProfile } from './types';

interface UsersState {
  userData: null | UserData;
  currentProfile: null | UserProfile;
  loading: LoadingType;
  loadingCurrentProfile: LoadingType;
  errorSignIn: string | undefined;
  errorSignUp: string | undefined;
  errorForgotPassword: string | undefined;
  errorUpdate: string | undefined;
  errorCurrentProfile: string | undefined;
  token: null | string;
  client: null | string;
}

const initialState: UsersState = {
  userData: null,
  currentProfile: null,
  loading: 'idle',
  loadingCurrentProfile: 'idle',
  errorSignIn: undefined,
  errorSignUp: undefined,
  errorForgotPassword: undefined,
  errorUpdate: undefined,
  errorCurrentProfile: undefined,
  token: null,
  client: null,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(userOperations.fetchSignIn.pending, (state) => {
      state.loading = 'pending';
    });
    builder.addCase(userOperations.fetchSignIn.fulfilled, (state, action) => {
      state.loading = 'succeeded';
      state.userData = action.payload.user;
      (state.token = action.payload.accessToken),
        (state.client = action.payload.client),
        (state.errorSignIn = undefined);
    });
    builder.addCase(userOperations.fetchSignIn.rejected, (state, action) => {
      state.loading = 'failed';
      state.errorSignIn = action.error.message;
    });
    builder.addCase(userOperations.fetchSignUp.pending, (state) => {
      state.loading = 'pending';
    });
    builder.addCase(userOperations.fetchSignUp.fulfilled, (state, action) => {
      state.loading = 'succeeded';
      state.userData = action.payload.user;
      (state.token = action.payload.accessToken),
        (state.client = action.payload.client),
        (state.errorSignUp = undefined);
    });
    builder.addCase(userOperations.fetchSignUp.rejected, (state, action) => {
      state.loading = 'failed';
      if (action.payload) {
        state.errorSignUp = action.payload.errors.full_messages[0];
      } else {
        state.errorSignUp = action.error.message;
      }
    });
    builder.addCase(userOperations.fetchForgotPassword.pending, (state) => {
      state.loading = 'pending';
    });
    builder.addCase(userOperations.fetchForgotPassword.fulfilled, (state) => {
      state.loading = 'succeeded';
    });
    builder.addCase(
      userOperations.fetchForgotPassword.rejected,
      (state, action) => {
        state.loading = 'failed';
        state.errorForgotPassword = action.error.message;
      },
    );
    builder.addCase(
      userOperations.fetchUpdateProfile.fulfilled,
      (state, action) => {
        state.currentProfile = action.payload.data.update_profile.profile;
      },
    );
    builder.addCase(
      userOperations.fetchUpdateProfile.rejected,
      (state, action) => {
        if (action.payload) {
          state.errorUpdate = action.payload.errorMessage;
        } else {
          state.errorUpdate = action.error.message;
        }
      },
    );
    builder.addCase(userOperations.fetchCurrentProfile.pending, (state) => {
      state.loadingCurrentProfile = 'pending';
    });
    builder.addCase(
      userOperations.fetchCurrentProfile.fulfilled,
      (state, action) => {
        state.loadingCurrentProfile = 'succeeded';
        state.currentProfile = action.payload.data.current_profile;
      },
    );
    builder.addCase(
      userOperations.fetchCurrentProfile.rejected,
      (state, action) => {
        state.loadingCurrentProfile = 'failed';
        state.errorCurrentProfile = action.error.message;
      },
    );
  },
});

export default userSlice.reducer;
