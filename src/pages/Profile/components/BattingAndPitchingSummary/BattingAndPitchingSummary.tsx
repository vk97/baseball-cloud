import React, { useMemo } from 'react';
import EmptyData from 'src/components/EmptyData';
import Table from 'src/components/Table';
import { BattingSummary, PitchingSummary } from 'src/types';
import './styles.scss';

interface BattingAndPitchingSummaryProps {
  type: 'Batting' | 'Pithing';
  columns: Array<{ Header: string; accessor: string }>;
  dataSummary: BattingSummary | PitchingSummary;
}

const BattingAndPitchingSummary = ({
  columns,
  type,
  dataSummary,
}: BattingAndPitchingSummaryProps): JSX.Element => {
  const dataTopValues = useMemo(() => dataSummary.top_values, []);
  const dataAverage = useMemo(() => dataSummary.average_values, []);
  return (
    <section className="table-summary">
      <h2 className="visually-hidden">{type} summary</h2>
      <div className="table-summary__table-group">
        <h3 className="table-summary__title">Top {type} Values</h3>
        <Table columns={columns} data={dataTopValues} />
        {dataTopValues.length === 0 && <EmptyData />}
      </div>
      <div className="table-summary__table-group">
        <h3 className="table-summary__title">Average {type} Values</h3>
        <Table columns={useMemo(() => columns, [columns])} data={dataAverage} />
        {dataAverage.length === 0 && <EmptyData />}
      </div>
    </section>
  );
};

export default BattingAndPitchingSummary;
