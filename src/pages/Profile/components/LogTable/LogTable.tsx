import {
  faAngleDoubleLeft,
  faAngleDoubleRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useMemo, useState } from 'react';
import { useAsyncCallback } from 'react-async-hook';
import { Form, Field } from 'react-final-form';
import ReactPaginate from 'react-paginate';
import Select from 'react-select';
import EmptyData from 'src/components/EmptyData';
import LoaderData from 'src/components/LoaderData';
import Table from 'src/components/Table';
import useDebounce from 'src/hooks/useDebounce';
import { customStylesSelect } from 'src/styles/customStyles';
import { BattingLog, PitcherLog } from 'src/types';
import FetchData from 'src/utils/FetchData';
import { getFilterValue } from 'src/utils/utils';
import './styles.scss';

interface LogTableProps {
  profileId: string;
  type: 'Batting' | 'Pithing';
  columns: Array<{ Header: string; accessor: string }>;
}

interface Filters {
  offset: 0;
  count: 10;
  name?: string;
}

const LogTable = ({ type, columns, profileId }: LogTableProps): JSX.Element => {
  const [data, setData] = useState<Array<BattingLog> | Array<PitcherLog> | []>(
    [],
  );
  const COUNT_VIEW = 10;
  const [filters, setFilters] = useState({ offset: 0, count: 10 });
  const [totalCount, setTotalCount] = useState<number>(0);
  const lengthPagination = Math.ceil(totalCount / COUNT_VIEW);
  const logData = useMemo(() => data, [data]);

  const optionsType = [
    { value: '', label: 'None' },
    { value: 'Four Seam Fastball', label: 'Four Seam Fastball' },
    { value: 'Two Seam Fastball', label: 'Two Seam Fastball' },
    { value: 'Curveball', label: 'Curveball' },
    { value: 'Changeup', label: 'Changeup' },
    { value: 'Slider', label: 'Slider' },
  ];

  const handleGetBattingLog = useAsyncCallback(
    () => {
      return FetchData.fetchBattingLog(profileId, filters);
    },
    {
      onSuccess: (result) => {
        if (result.data.data) {
          setData(result.data.data.batting_log.batting_log);
          setTotalCount(result.data.data.batting_log.total_count);
        }
      },
    },
  );

  const handleGetPithingLog = useAsyncCallback(
    () => {
      return FetchData.fetchPithingLog(profileId, filters);
    },
    {
      onSuccess: (result) => {
        if (result.data.data) {
          setData(result.data.data.pitching_log.pitching_log);
          setTotalCount(result.data.data.pitching_log.total_count);
        }
      },
    },
  );

  const debouncedSearchLog = useDebounce(filters, 500);

  useEffect(() => {
    setData([]);

    if (debouncedSearchLog) {
      if (type === 'Batting') {
        handleGetBattingLog.execute();
      } else {
        handleGetPithingLog.execute();
      }
    }

    return () => {
      handleGetBattingLog.reset();
      handleGetPithingLog.reset();
    };
  }, [debouncedSearchLog]);

  return (
    <section className="table-log">
      <h2 className="table-log__title">{type} log</h2>
      <Form
        onSubmit={(filters: Filters) => setFilters(filters)}
        render={({ handleSubmit }) => (
          <form className="tab-log-filters" onSubmit={handleSubmit}>
            <Field
              name={type === 'Batting' ? 'pitcher_name' : 'batter_name'}
              type="text"
              render={({ input }) => (
                <div className="tab-log-filters__input-search input-search">
                  <div className="input-search__container">
                    <input
                      className="input-search__input"
                      {...input}
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                      placeholder="Search"
                    />
                  </div>
                </div>
              )}
            />
            <Field
              name="pitch_type"
              type="text"
              parse={getFilterValue}
              render={({ input }) => (
                <Select
                  styles={customStylesSelect}
                  options={optionsType}
                  placeholder="Pitch Type"
                  onChange={(value) => {
                    input.onChange(value);
                    handleSubmit();
                  }}
                  isSearchable={false}
                />
              )}
            />
          </form>
        )}
      />
      {(handleGetPithingLog.loading || handleGetBattingLog.loading) && (
        <LoaderData />
      )}
      {!handleGetPithingLog.loading && !handleGetBattingLog.loading && (
        <Table columns={columns} data={logData} />
      )}
      {!handleGetPithingLog.loading &&
        !handleGetBattingLog.loading &&
        data.length === 0 && <EmptyData />}

      {(data.length > 0 || lengthPagination !== 0) && (
        <ReactPaginate
          previousLabel={
            <FontAwesomeIcon
              icon={faAngleDoubleLeft}
              color="#667784"
              width="6"
              height="6"
            />
          }
          nextLabel={
            <FontAwesomeIcon
              icon={faAngleDoubleRight}
              color="#667784"
              width="6"
              height="6"
            />
          }
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={lengthPagination}
          marginPagesDisplayed={1}
          pageRangeDisplayed={3}
          onPageChange={({ selected }) =>
            setFilters({
              ...filters,
              offset: selected * filters.count,
            })
          }
          containerClassName="pagination"
          pageClassName="page-item"
          pageLinkClassName="page-link"
          activeClassName={'active'}
        />
      )}
    </section>
  );
};

export default LogTable;
