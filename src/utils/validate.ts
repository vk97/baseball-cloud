export const required = (value: string | number): undefined | string =>
  value ? undefined : 'Required';
