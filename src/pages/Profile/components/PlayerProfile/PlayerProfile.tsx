import React from 'react';
import SideBarLeft from 'src/components/SideBarLeft';
import { PlayerProfile as PlayerProfileType } from 'src/redux/data/types';
import { checkPositionPitcher } from 'src/utils/utils';
import ProfileInfo from '../ProfileInfo';
import UserProfileReport from '../UserProfileReport';

interface PlayerProfileProps {
  userProfile: PlayerProfileType;
}

const PlayerProfile = ({
  userProfile,
}: PlayerProfileProps): JSX.Element => {
  return (
    <>
      <SideBarLeft>
        <ProfileInfo profile={userProfile} />
      </SideBarLeft>
      <UserProfileReport
        userProfile={userProfile}
        isPitcher={checkPositionPitcher(userProfile)}
        isCurrentProfile={false}
      />
    </>
  );
};

export default PlayerProfile;
