import { LoadingType } from 'src/types';
import { RootState } from '../reducer';
import { UserData } from './types';

const selectUserData = (state: RootState): UserData | null =>
  state.USER.userData;
const selectCurrentProfile = (state: RootState): any =>
  state.USER.currentProfile;
const selectStatusLoading = (state: RootState): LoadingType =>
  state.USER.loading;
const selectStatusLoadingCurrentProfile = (
  state: RootState,
): LoadingType => state.USER.loadingCurrentProfile;
const selectToken = (state: RootState): null | string => {
  return state.USER.token;
};
const selectErrorSignIn = (state: RootState): string | undefined =>
  state.USER.errorSignIn;
const selectErrorSignUp = (state: RootState): string | undefined =>
  state.USER.errorSignUp;
const selectErrorForgotPassword = (
  state: RootState,
): string | undefined => state.USER.errorForgotPassword;

export default {
  selectUserData,
  selectCurrentProfile,
  selectStatusLoading,
  selectStatusLoadingCurrentProfile,
  selectToken,
  selectErrorSignIn,
  selectErrorSignUp,
  selectErrorForgotPassword,
}
