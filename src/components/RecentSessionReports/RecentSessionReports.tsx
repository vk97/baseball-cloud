import React from 'react';
import { RecentEventType } from 'src/types';
import Card from '../Card';
import LoaderData from '../LoaderData';
import SessionReportLists from '../SessionReportLists';
import './styles.scss';

export interface RecentSessionReportsProfile {
  id: string;
  recent_events: [] | RecentEventType[];
  [key: string]: any;
}

interface RecentSessionReportsProps {
  profileList: RecentSessionReportsProfile[];
  isLoading: boolean;
}

const RecentSessionReports = ({
  profileList,
  isLoading,
}: RecentSessionReportsProps): JSX.Element => {
  return (
    <Card title="Recent Session Reports" classElement="recent-events">
      {isLoading && <LoaderData />}
      <SessionReportLists profilesList={profileList} isLoading={isLoading} />
    </Card>
  );
};

RecentSessionReports.defaultProps = {
  isLoding: false,
} as Partial<RecentSessionReportsProps>;

export default RecentSessionReports;
