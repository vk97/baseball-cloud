import React, { useEffect, useMemo, useState } from 'react';
import MainContainer from 'src/components/MainContainer';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Table from 'src/components/Table';
import { LoadingType } from 'src/types';
import {
  POSITION_VALUES,
  QUERY_LEADER_BOARD_BATTING,
  QUERY_LEADER_BOARD_PITCHING,
} from 'src/const';
import { fetchApi } from 'src/utils/fetchApi';
import { toast } from 'react-toastify';
import LoaderData from 'src/components/LoaderData';
import { Form, Field } from 'react-final-form';
import { getFilterValue } from 'src/utils/utils';
import Select from 'react-select';
import { customStylesSelect } from 'src/styles/customStyles';
import FilterInput from 'src/components/FilterInput';
import EmptyData from 'src/components/EmptyData';
import {
  LeaderBoardBattingResponse,
  LeaderBoardPitchingResponse,
  LeaderBoardType,
} from './types';
import './styles.scss';
import { useAsyncCallback } from 'react-async-hook';
import FetchData from 'src/utils/FetchData';

const Leaderboard = (): JSX.Element => {
  const [leaderBoard, setLeaderBoard] = useState<LeaderBoardType>([]);
  const [initialValuesFilter, setInitialValuesFilter] = useState({
    type: 'exit_velocity',
  });
  const [loading, setLoading] = useState<LoadingType>('idle');
  const [tabIndex, setTabIndex] = useState(0);

  const fetchLeaderBoardBatting = async (filterData: any) => {
    const data = {
      query: QUERY_LEADER_BOARD_BATTING,
      variables: {
        input: filterData,
      },
    };

    setLoading('pending');
    const response = await fetchApi<LeaderBoardBattingResponse>(
      '/api/v1/graphql',
      data,
    );
    if (response.status === 200) {
      const leader = response.data.data.leaderboard_batting.leaderboard_batting;
      setLeaderBoard(leader);
      setInitialValuesFilter({ type: 'exit_velocity' });
      setLoading('succeeded');
    } else if (response.data.data.leaderboard_batting.leaderboard_batting) {
      setLoading('failed');
      toast.error(`Error request.`);
    }
  };

  const fetchLeaderBoardPitching = async (filterData: any) => {
    const data = {
      query: QUERY_LEADER_BOARD_PITCHING,
      variables: {
        input: filterData,
      },
    };

    setLoading('pending');
    const response = await fetchApi<LeaderBoardPitchingResponse>(
      '/api/v1/graphql',
      data,
    );
    if (response.status === 200) {
      const leader =
        response.data.data.leaderboard_pitching.leaderboard_pitching;
      setLeaderBoard(leader);
      setInitialValuesFilter({ type: 'pitch_velocity' });
      setLoading('succeeded');
    } else {
      setLoading('failed');
      toast.error(`Error request.`);
    }
  };

  const handleUpdateFavorite = useAsyncCallback(
    (id: string, favorite: boolean) => {
      return FetchData.fetchUpdateFavorite(id, !favorite);
    },
    {
      onSuccess(result) {
        if (result.status === 200 && result.data.data.update_favorite_profile) {
          if (tabIndex === 0) {
            fetchLeaderBoardBatting(initialValuesFilter);
          } else {
            fetchLeaderBoardPitching(initialValuesFilter);
          }
          const isFavorite = result.data.data.update_favorite_profile.favorite;
          toast.success(
            `This profile ${
              isFavorite ? 'added' : 'removed'
            } to favorite list successfully.`,
          );
        }
      },
    },
  );

  useEffect(() => {
    if (tabIndex === 0) {
      fetchLeaderBoardBatting({ type: 'exit_velocity' });
    } else {
      fetchLeaderBoardPitching({ type: 'pitch_velocity' });
    }
    return () => {
      setLeaderBoard([]);
    };
  }, [tabIndex]);

  const columnsBattingTable = useMemo(
    () => [
      { Header: 'Rank', accessor: 'col1' },
      { Header: 'Batter Name', accessor: 'batter_name' },
      { Header: 'Age', accessor: 'age' },
      { Header: 'School', accessor: 'school' },
      { Header: 'Teams', accessor: 'teams' },
      { Header: 'Exit Velocity', accessor: 'exit_velocity' },
      { Header: 'Launch Angle', accessor: 'launch_angle' },
      { Header: 'Distance', accessor: 'distance' },
      { Header: 'Favorite', accessor: 'favorite' },
    ],
    [],
  );

  const columnsPitchingTable = useMemo(
    () => [
      { Header: 'Rank', accessor: 'rank' },
      { Header: 'Pitcher Name', accessor: 'pitcher_name' },
      { Header: 'Age', accessor: 'age' },
      { Header: 'School', accessor: 'school' },
      { Header: 'Teams', accessor: 'teams' },
      { Header: 'Pitch Type', accessor: 'pitch_type' },
      { Header: 'Velocity', accessor: 'velocity' },
      { Header: 'Spin Rate', accessor: 'spinRate' },
      { Header: 'Favorite', accessor: 'favorite' },
    ],
    [],
  );

  const dateFilterOptions = [
    { value: 'all', label: 'All' },
    { value: 'last_week', label: 'Last Week' },
    { value: 'last_month', label: 'Last Month' },
  ];

  const typeOptionsBatting = [
    { value: 'exit_velocity', label: 'Exit Velocity' },
    { value: 'carry_distance', label: 'Carry Distance' },
  ];

  const typeOptionsPitching = [
    { value: 'pitch_velocity', label: 'Pitch Velocity' },
    { value: 'spin_rate', label: 'Spin Rate' },
  ];

  return (
    <MainContainer>
      <section className="leaderboard">
        <div className="leaderboard__header">
          <h2 className="leaderboard__title">Leaderboard</h2>
          <Form
            initialValues={initialValuesFilter}
            onSubmit={(filterData) => {
              if (tabIndex === 0) {
                fetchLeaderBoardBatting(filterData);
              } else {
                fetchLeaderBoardPitching(filterData);
              }
            }}
            render={({ handleSubmit, values }) => (
              <form className="form-filters" onSubmit={handleSubmit}>
                <Field
                  name="date"
                  type="text"
                  placeholder="Date"
                  parse={getFilterValue}
                  render={({ input, placeholder }) => (
                    <Select
                      styles={customStylesSelect}
                      options={dateFilterOptions}
                      placeholder={placeholder}
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                      isSearchable={false}
                    />
                  )}
                />
                <Field
                  name="school"
                  type="text"
                  render={({ input }) => (
                    <FilterInput
                      restProps={{ ...input }}
                      minLength={3}
                      maxLength={35}
                      placeholder="School"
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                    />
                  )}
                />
                <Field
                  name="team"
                  type="text"
                  render={({ input }) => (
                    <FilterInput
                      restProps={{ ...input }}
                      minLength={3}
                      maxLength={35}
                      placeholder="Team"
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                    />
                  )}
                />
                <Field
                  name="position"
                  type="text"
                  parse={getFilterValue}
                  render={({ input }) => (
                    <Select
                      styles={customStylesSelect}
                      options={[
                        {
                          value: '',
                          label: 'All',
                        },
                        ...POSITION_VALUES,
                      ]}
                      placeholder="Position"
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                      isSearchable={false}
                    />
                  )}
                />
                <Field
                  name="age"
                  type="text"
                  parse={(value) => value ? value : undefined}
                  render={({ input }) => (
                    <FilterInput
                      restProps={{ ...input }}
                      minLength={1}
                      maxLength={3}
                      placeholder="Age"
                      onChange={(value) => {
                        const age = Number(value);
                        if (!isNaN(age)) {
                          input.onChange(age);
                          handleSubmit();
                        }
                      }}
                    />
                  )}
                />

                <Field
                  name="favorite"
                  type="text"
                  parse={(option) => (!option.value ? undefined : option.value)}
                  render={({ input }) => (
                    <Select
                      styles={customStylesSelect}
                      options={[
                        { value: '', label: 'All' },
                        { value: 1, label: 'Favorite' },
                      ]}
                      placeholder="All"
                      onChange={(value) => {
                        input.onChange(value);
                        handleSubmit();
                      }}
                      isSearchable={false}
                    />
                  )}
                />

                {tabIndex === 0 ? (
                  <Field
                    name="type"
                    type="text"
                    placeholder="Exit Velocity"
                    parse={getFilterValue}
                    render={({ input, placeholder }) => (
                      <div className="form-filters__select-container">
                        <Select
                          value={typeOptionsBatting.find(
                            (option) => option.value === values.type,
                          )}
                          styles={customStylesSelect}
                          placeholder={placeholder}
                          options={typeOptionsBatting}
                          onChange={(value) => {
                            input.onChange(value);
                            handleSubmit();
                          }}
                          isSearchable={false}
                        />
                      </div>
                    )}
                  />
                ) : (
                  <Field
                    name="type"
                    type="text"
                    placeholder="Pitch Velocity"
                    parse={getFilterValue}
                    render={({ input, placeholder }) => (
                      <div className="form-filters__select-container">
                        <Select
                          styles={customStylesSelect}
                          value={typeOptionsPitching.find(
                            (option) => option.value === values.type,
                          )}
                          placeholder={placeholder}
                          options={typeOptionsPitching}
                          onChange={(value) => {
                            input.onChange(value);
                            handleSubmit();
                          }}
                          isSearchable={false}
                        />
                      </div>
                    )}
                  />
                )}
              </form>
            )}
          />
        </div>
        <Tabs
          selectedIndex={tabIndex}
          onSelect={(index) => setTabIndex(index)}
          className="leaderboard__tabs">
          <TabList>
            <Tab>Batting</Tab>
            <Tab>Pitching</Tab>
          </TabList>

          <TabPanel>
            <Table
              onChangeStatusFavorite={handleUpdateFavorite}
              columns={columnsBattingTable}
              data={useMemo(() => leaderBoard, [leaderBoard])}
            />
            {loading === 'pending' && <LoaderData />}
            {leaderBoard.length === 0 && loading === 'succeeded' && (
              <EmptyData />
            )}
          </TabPanel>
          <TabPanel>
            <Table
              onChangeStatusFavorite={handleUpdateFavorite}
              columns={columnsPitchingTable}
              data={useMemo(() => leaderBoard, [leaderBoard])}
            />
            {loading === 'pending' && <LoaderData />}
            {leaderBoard.length === 0 && loading === 'succeeded' && (
              <EmptyData />
            )}
          </TabPanel>
        </Tabs>
      </section>
    </MainContainer>
  );
};

export default Leaderboard;
