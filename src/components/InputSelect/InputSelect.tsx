import React from 'react';

interface InputSelectProps {
  placeholder: string;
  [x: string]: any;
}

const InputSelect = ({
  placeholder,
  ...inputProps
}: InputSelectProps): JSX.Element => {
  return (
    <div className="select-block">
      <div className="select-block__placeholder">
        {placeholder}
      </div>
    </div>
  )
};
