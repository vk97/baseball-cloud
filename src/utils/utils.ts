import { months, POSITION_VALUES } from 'src/const';
import { PlayerProfile } from 'src/redux/data/types';
import { UserProfile } from 'src/redux/user/types';

export const getDate = (
  date: string,
): { day: string; month: string; year: number } => {
  const dateProps: Date = new Date(date);
  const month = dateProps.getMonth();
  const day = String(dateProps.getUTCDate()).padStart(2, '0');
  const year = dateProps.getUTCFullYear();
  return { day, month: months[month], year };
};

export const checkPositionPitcher = (userProfile: PlayerProfile): boolean => {
  const isPitcher =
    userProfile.position === 'pitcher' || userProfile.position2 === 'pitcher';

  return isPitcher;
};

export const checkCompletedProfileData = (
  userProfile: PlayerProfile,
): boolean => {
  const isNoEmptyData =
    !!userProfile.first_name &&
    !!userProfile.last_name &&
    !!userProfile.age &&
    !!userProfile.feet &&
    !!userProfile.weight &&
    !!userProfile.throws_hand &&
    !!userProfile.bats_hand;

  return isNoEmptyData;
};

export const convertStringToNumber = (
  value: string | number,
): string | number => {
  if (!value) return value;

  return Number(value);
};

export const getLabelPosition = (value: string | null): string => {
  if (!value) {
    return '';
  }

  const position = POSITION_VALUES.find((position) => position.value === value);
  if (position) {
    return position.label;
  } else {
    return '';
  }
};

export const getFilterValue = (option: {value: string, label:string}): string => {
  if (!option) {
    return '';
  }

  return option.value;
}
