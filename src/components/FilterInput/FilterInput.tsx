import React, { useRef } from 'react';
import IconArrowDown from '../Icons/IconArrowDown';
import './styles.scss';

interface restProps {
  [key: string]: string;
}
interface FilterInputProps {
  restProps: restProps;
  placeholder?: string;
  minLength: number;
  maxLength: number;
  onChange: (value: string) => void;
}

const FilterInput = ({
  placeholder,
  minLength,
  maxLength,
  onChange,
  restProps,
}: FilterInputProps): JSX.Element => {
  const inputRef = useRef<HTMLInputElement>(null);

  const handleFocus = () => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  };

  return (
    <div className="filter-input">
      <input
        className="filter-input__input"
        placeholder={placeholder}
        ref={inputRef}
        {...restProps}
        minLength={minLength}
        maxLength={maxLength}
        onChange={() => {
          if (inputRef.current) {
            onChange(inputRef.current.value);
          }
        }}
      />

      <span className="filter-input__icon" onClick={handleFocus}>
        <IconArrowDown width={16} height={9} color={'#48bbff'} />
      </span>
    </div>
  );
};

FilterInput.defaaultProps = {
  placeholder: '',
} as Partial<FilterInputProps>;

export default FilterInput;
