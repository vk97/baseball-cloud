import {
  Facility,
  RecentEventType,
  School,
  Team,
  Teams,
} from 'src/types';

const GET_TEAMS = 'user/getTeams';
const GET_SCHOOLS = 'user/getSchools';
const GET_FACILITIES = 'user/getFacilities';

export interface Data {
  teams: Array<Team> | null;
  schools: Array<School> | null;
  facilities: Array<Facility> | null;
  error: undefined | string;
}

export interface TeamsResponse {
  data: {
    teams: {
      teams: Array<Team>;
    };
  };
}

export interface SchoolsResponse {
  data: {
    schools: {
      schools: Array<School>;
    };
  };
}

export interface FacilitiesResponse {
  data: {
    facilities: {
      facilities: Array<Facility>;
    };
  };
}

export interface PlayerProfile {
  act_score: number;
  age: number;
  avatar: string | null;
  bats_hand: string;
  batter_summary: [];
  batting_top_values: [];
  biography: string;
  broad_jump: number;
  events_opened: true;
  facilities: [];
  favorite: boolean;
  feet: number;
  first_name: string;
  grip_right: number;
  id: string;
  inches: number;
  last_name: string;
  paid: false;
  pitcher_summary: [];
  pitching_top_values: [];
  position: string;
  position2: string;
  recent_events: [] | RecentEventType[];
  sat_score: number;
  school: School;
  school_year: string;
  teams: Teams;
  throws_hand: string;
  weight: number;
  winsgspan: number;
  wrist_to_elbow: number;
}

export default {
  GET_TEAMS,
  GET_SCHOOLS,
  GET_FACILITIES,
}
