import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faReply } from '@fortawesome/free-solid-svg-icons';
import './styles.scss';

const ProfileNotCompleted = (): JSX.Element => {
  return (
    <div className="user-profile__no-data no-data">
      <div className="no-data__container">
        <FontAwesomeIcon
          icon={faReply}
          color="#48bbff"
          width="52"
          height="47"
          className="no-data__icon"
        />
        <h2 className="no-data__header">Your Account</h2>
        <p className="no-data__text">
          Changing your profile options lets you control how others see you and
          your profile. These settings include things like your name, personal
          info and school.
        </p>
      </div>
    </div>
  );
};

export default ProfileNotCompleted;
