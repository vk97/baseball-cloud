import React from 'react';
import { createBrowserHistory } from 'history';
import Navigation from './pages/Navigation';

export const customHistory = createBrowserHistory();

const App: React.FC = () => {
  return <Navigation />;
};

export default App;
