import React from 'react';
import './styles.scss';

interface CardProps {
  title: string;
  titleHide: boolean;
  classElement: string;
  children: React.ReactNode;
}

const Card = ({
  title,
  titleHide,
  classElement,
  children,
}: CardProps): JSX.Element => {
  return (
    <section className={`card ${classElement}`}>
      <h2 className={`card__title ${titleHide && 'visually-hidden'}`}>
        {title}
      </h2>
      {children}
    </section>
  );
};

Card.defaultProps = {
  titleHide: false,
  classElement: '',
} as Partial<CardProps>

export default Card;
