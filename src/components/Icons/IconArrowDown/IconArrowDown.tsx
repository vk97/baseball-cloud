import React from 'react';

interface IconArrowDownProps {
  width?: number;
  height?: number;
  color?: string;
}

const IconArrowDown = ({
  width,
  height,
  color,
}: IconArrowDownProps): JSX.Element => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox="0 0 16 9">
      <path
        fill={color}
        fillRule="nonzero"
        d="M13.469.432a1.081 1.081 0 0 1 1.565 0 1.165 1.165 0 0 1 0 1.615L8.78 8.43a1.083 1.083 0 0 1-1.567 0L.962 2.047a1.168 1.168 0 0 1 0-1.615 1.081 1.081 0 0 1 1.564 0L8 5.667 13.469.432z"></path>
    </svg>
  );
};

IconArrowDown.defaultProps = {
  width: 16,
  height: 9,
  color: '#788b99',
} as Partial<IconArrowDownProps>;

export default IconArrowDown;
