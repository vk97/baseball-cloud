import { createAsyncThunk } from '@reduxjs/toolkit';
import { QUERY_FACILITIES, QUERY_SCHOOLS, QUERY_TEAMS } from 'src/const';
import { FetchApi } from 'src/types';
import { RootState } from '../reducer';
import { AppDispatch } from '../store';
import {
  FacilitiesResponse,
  SchoolsResponse,
  TeamsResponse,
} from './types';
import dataActionTypes from './types';

const fetchTeams = createAsyncThunk<
  TeamsResponse,
  unknown,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: FetchApi<TeamsResponse>;
  }
>(dataActionTypes.GET_TEAMS, async (unknown, thunkApi) => {
  const data = {
    query: QUERY_TEAMS,
    variables: {
      search: '',
    },
  };

  const response = await thunkApi.extra('/api/v1/graphql', data);

  return response.data as TeamsResponse;
});

const fetchSchools = createAsyncThunk<
  SchoolsResponse,
  unknown,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: FetchApi<SchoolsResponse>;
  }
>(dataActionTypes.GET_SCHOOLS, async (unknown, thunkApi) => {
  const data = {
    query: QUERY_SCHOOLS,
    variables: {
      search: '',
    },
  };

  const response = await thunkApi.extra('/api/v1/graphql', data);

  return response.data as SchoolsResponse;
});

const fetchFacilities = createAsyncThunk<
  FacilitiesResponse,
  unknown,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: FetchApi<FacilitiesResponse>;
  }
>(dataActionTypes.GET_FACILITIES, async (unknown, thunkApi) => {
  const data = {
    query: QUERY_FACILITIES,
    variables: {
      search: '',
    },
  };

  const response = await thunkApi.extra('/api/v1/graphql', data);

  return response.data as FacilitiesResponse;
});

export default {
  fetchTeams,
  fetchSchools,
  fetchFacilities,
};
