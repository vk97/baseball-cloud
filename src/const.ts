export const months: Array<string> = [
  'jan',
  'feb',
  'mar',
  'apr',
  'may',
  'jun',
  'jul',
  'aug',
  'sep',
  'oct',
  'nov',
  'dec',
];

export const POSITION_VALUES = [
  { value: 'catcher', label: 'Catcher' },
  { value: 'first_base', label: 'First Base' },
  { value: 'second_base', label: 'Second Base' },
  { value: 'shortstop', label: 'Shortstop' },
  { value: 'third_Base', label: 'Third Base' },
  { value: 'outfield', label: 'Outfield' },
  { value: 'pitcher', label: 'Pitcher' },
];

export const QUERY_CURRENT_PROFILE =
  '{ current_profile () {id  first_name  last_name position position2 avatar throws_hand  bats_hand biography school_year feet inches weight age  school {id name} teams {id name}  facilities { id email u_name }}}';
export const QUERY_PROFILE = `query Profile($id:String!)
{ profile(id: $id)
  {
    id
    first_name
    last_name
    position
    position2
    school_year
    avatar
    throws_hand
    bats_hand
    biography
    feet
    inches
    weight
    age
    recent_events {
  id
  event_type
  event_name
  date
  is_pitcher
  data_rows_count
  recent_avatars {
    id
    first_name
    last_name
    avatar
  }
    }
    winsgspan
    grip_right
    grip_left
    wrist_to_elbow
    broad_jump
    grip_left
    act_score
    gpa_score
    sat_score
    batting_top_values {
  pitch_type
  distance
  launch_angle
  exit_velocity
    }
    pitching_top_values {
  velocity
  spin_rate
  pitch_type
    }
    pitcher_summary {
  velocity
  spin_rate
  horizontal_break
    }
    batter_summary {
  exit_velocity
  distance
  launch_angle
    }
    school {
  id
  name
    }
    teams {
  id
  name
    }
    facilities {
  id
  email
  u_name
    }
    favorite
    events_opened
    paid
  }
}`;
export const QUERY_TEAMS =
  'query Teams($search:String!) { teams(search: $search) { teams { id name }}}';
export const QUERY_SCHOOLS =
  'query Schools($search:String!) { schools(search: $search) { schools { id name }}}';
export const QUERY_FACILITIES =
  'query Facilities($search:String!) { facilities(search: $search) { facilities { id email u_name }}}';
export const QUERY_UPDATE_PROFILE = `mutation UpdateProfile($form:UpdateProfileInput!)
{ update_profile (input:$form)
  { profile
    {
  id
  first_name
  last_name
  position
  position2
  avatar
  throws_hand
  bats_hand
  biography
  school_year
  feet
  inches
  weight
  age
  school {
    id
    name
  }
  teams {
    id
    name
  }
  facilities {
    id
    email
    u_name
  }
    }
  }
}`;

export const QUERY_LEADER_BOARD_BATTING = `query LeaderboardBatting($input:FilterLeaderboardInput!)
{ leaderboard_batting(input: $input)
    { leaderboard_batting {
    batter_name
    exit_velocity
    launch_angle
    distance
    batter_datraks_id
    age
    school {
  id
  name
    }
    teams {
  id
  name
    }
    favorite
  }
    }
}`;

export const QUERY_LEADER_BOARD_PITCHING = `query LeaderboardPitching($input:FilterLeaderboardInput!)
{ leaderboard_pitching(input: $input)
    { leaderboard_pitching
  {
    pitcher_name
    pitch_type
    velocity
    spin_rate
    vertical_break
    horizontal_break
    pitcher_datraks_id
    age
    school {
  id
  name
    }
    teams {
  id
  name
    }
    favorite
  }
    }
}`;

export const QUERY_PROFILES_NETWORK = `query Profiles($input:FilterProfilesInput!)
{ profiles(input: $input)
  { profiles
    {
  id
  first_name
  last_name
  position
  position2
  school_year
  feet
  inches
  weight
  age
  events {
    id
  }
  school {
    id
    name
  }
  teams {
    id
    name
  }
  favorite
    }
    total_count
  }
}`;
export const QUERY_PROFILE_EVENTS = `query ProfileEvents($input:FilterProfileEventsInput!) { profile_events(input: $input) {  events { id date event_type event_name   }   total_count }   }`;
export const QUERY_BATTING_SUMMARY = `query BattingSummary($id:ID!) { batting_summary(id: $id) { top_values {id distance pitch_type launch_angle exit_velocity } average_values{ id distance pitch_type launch_angle exit_velocity   } }   }`;
export const QUERY_BATTING_GRAPH = `query BattingGraph($input:FilterGraphInput!)   { batting_graph(input: $input) { graph_rows   } }`;
export const QUERY_BATTING_LOG = `query BattingLog($input:FilterBattingLogInput!)   { batting_log(input: $input) {   batting_log { date pitcher_name pitcher_handedness pitch_type pitch_call exit_velocity launch_angle direction distance hit_spin_rate hang_time pitcher_datraks_id   }   total_count }   }`;
export const QUERY_PITCHING_SUMMARY = `query PitchingSummary($id:ID!)
{ pitching_summary(id: $id) {
    top_values {
  id
  velocity
  spin_rate
  pitch_type
    }
    average_values{
  id
  velocity
  spin_rate
  pitch_type
    }
  }
}`;

export const QUERY_PITCHING_GRAPH = `query PitchingGraph($input:FilterGraphInput!)  { pitching_graph(input: $input) {    graph_rows  }  }"
variables: {input: {profile_id: "413"}}`;
export const QUERY_PITCHING_LOG = `query PitchingLog($input:FilterPitchingLogInput!)   { pitching_log(input: $input) {   pitching_log { date pitch_type pitch_call velocity spin_rate spin_axis tilt release_height release_side extension vertical_break horizontal_break height_at_plate batter_name batter_datraks_id batter_handedness   }   total_count }   }`;
export const QUERY_PROFILE_NAMES = `query ProfileNames($input:FilterProfileNamesInput!)   { profile_names(input: $input) {   profile_names { id position first_name last_name inches feet weight age   } }   }`;
export const QUERY_FAVORITE_PROFILES = `query MyFavoriteProfiles($input:FilterProfilesInput!) { my_favorite(input: $input) { profiles {   id   first_name   last_name   recent_events { id event_type event_name date recent_avatars {   id   first_name   last_name   avatar }   } } total_count   } }`;
export const QUERY_CHANGE_FAVORITE_PROFILE = `mutation UpdateFavoriteProfile($form:UpdateFavoriteProfileInput!) {    update_favorite_profile(input: $form) {      favorite    }  }`;
export const QUERY_PROFILES_IN_EVENT = `query Profiles($input:FilterProfilesInput!)       { profiles(input: $input)  { profiles    {      id      first_name      last_name      position      position2      school_year      feet      inches      weight      age      events {        id      }      school {        id        name      }      teams {        id        name      }      favorite    }    total_count  }       }`;

export const QUERY_EVENT_DETAIL = `query EventDetail($input:FilterEventDetailInput!)        { event_detail(input: $input) {    event {      id      date      event_type      event_name    }    is_pitcher    data_rows_count    profile    opened  }        }`;
export const QUERY_EVENT_SUMMARY = `query EventPlayerSummary($input:FilterEventDetailInput!)    { event_player_summary(input: $input) {        hitting_summary_table_rows {          pitch_type          distance          direction          launch_angle          exit_velocity          pitch_call        }        pitching_summary_table_rows {          pitch_count          pitch_type          ball_count          called_strike_count          swing_strike_count          avg_velo          max_velo          avg_spin          max_spin        }        pitching_zone_chart_rows {          pitch_type          side_at_plate          height_at_plate        }        hitting_zone_chart_rows {          pitch_type          side_at_plate          height_at_plate        }        release_point_chart_rows {          pitch_type          release_side          release_height        }        hitter_unique_pitches        pitcher_unique_pitches      }    }`;
export const QUERY_EVENT_ROWS = `query EventRows($input:FilterEventRowsInput!)    { event_rows(input: $input) {        event_rows {          id          pitcher_name          pitcher_handedness          batter_name          pitch_type          pitch_call          velocity          spin_rate          release_side          extension          vertical_break          horizontal_break          height_at_plate          exit_velocity          launch_angle          direction          hit_spin_rate          distance          hang_time        }        total_count      }    }`;
export const QUERY_EVENT_HITTING_SUMMARY = `query EventHittingSummary($input:FilterEventDetailInput!)    { event_hitting_summary(input: $input) {        hitting_summary_rows {          batter_name          pitcher_name          pitch_type          exit_velocity          launch_angle          distance          pitch_call        }      }    }`;
export const QUERY_EVENT_PITCHING_SUMMARY = `query EventPitchingSummary($input:FilterEventDetailInput!)    { event_pitching_summary(input: $input) {        pitching_summary_rows {          pitcher_name          pitcher_team          pitch_count          pitch_type          ball_count          called_strike_count          swing_strike_count          avg_velo          max_velo          avg_spin          max_spin        }        velocity_chart_rows {          pitcher_name          pitcher_team          pitch_type          velo        }      }    }`;

