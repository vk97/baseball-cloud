import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import logo from 'src/assets/images/icon-logo.svg';
import { userSelectors } from 'src/redux/user';
import Profile from '../Profile';
import './styles.scss';

const Header = (): JSX.Element => {
  const userData = useSelector(userSelectors.selectUserData);

  return (
    <header className="page-header">
      <div className="page-header__logo">
        <a href="/" className="page-header__logo-link">
          <img
            src={logo}
            width={198}
            height={28}
            alt="Baseball cloud"
            className="page-header__image"
          />
        </a>
      </div>
      {userData && (
        <div className="page-header__right">
          <nav className="page-header__nav main-nav">
            <ul className="main-nav__lists nav-lists">
              <li className="nav-lists__item">
                <Link to="/leaderboard" className="nav-lists__link">
                  Leaderboard
                </Link>
              </li>
              <li className="nav-lists__item">
                <Link to="/network" className="nav-lists__link">
                  Network
                </Link>
              </li>
            </ul>
          </nav>
          <Profile userData={userData} />
        </div>
      )}
    </header>
  );
};

export default Header;
