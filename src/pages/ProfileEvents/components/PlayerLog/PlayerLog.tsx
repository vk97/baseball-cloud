import React, { useEffect, useMemo, useState } from 'react';
import Card from 'src/components/Card';
import TableCustom from 'src/components/Table';
import { useAsync } from 'react-async-hook';
import FetchData from 'src/utils/FetchData';
import Select from 'react-select';
import { customStylesSelect } from 'src/styles/customStyles';
import {
  faAngleDoubleLeft,
  faAngleDoubleRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ReactPaginate from 'react-paginate';
import LoaderData from 'src/components/LoaderData';
import 'bootstrap/js/dist/collapse';
import './styles.scss';

interface PlayerLogProps {
  idEvent: string;
  idProfile: string;
}

interface Filters {
  count: number;
  offset: number;
}

const PlayerLog = ({ idEvent, idProfile }: PlayerLogProps): JSX.Element => {
  const [totalCount, setTotalCount] = useState<number>(0);
  const [filters, setFilters] = useState<Filters>({ count: 10, offset: 0 });

  const asyncPlayerLog = useAsync(
    FetchData.fetchEventPlayerLog,
    [idEvent, idProfile, filters],
    {
      onSuccess(result) {
        setTotalCount(result.data.data.event_rows.total_count);
        return result;
      },
    },
  );

  useEffect(() => {
    return () => {
      asyncPlayerLog.reset();
    };
  }, []);

  const lengthPagination = Math.ceil(totalCount / filters.count);

  const optionsSelect = [
    { value: 10, label: '10' },
    { value: 15, label: '15' },
    { value: 25, label: '25' },
  ];

  const dataTable = useMemo(() => {
    return asyncPlayerLog.result?.data.data.event_rows.event_rows || [];
  }, [asyncPlayerLog.result]);
  const columnTable = [
    { Header: 'Release Side', accessor: 'release_side' },
    { Header: 'Extension', accessor: 'extension' },
    { Header: 'Vertical Break', accessor: 'vertical_break' },
    {
      Header: 'Horizontal Break',
      accessor: 'horizontal_break',
    },
    {
      Header: 'Height At Plate',
      accessor: 'height_at_plate',
    },
    { Header: 'Exit Velocity', accessor: 'exit_velocity' },
    { Header: 'Launch Angle', accessor: 'launch_angle' },
    { Header: 'Direction', accessor: 'direction' },
    { Header: 'Hit Spin Rate', accessor: 'hit_spinRate' },
    { Header: 'Distance', accessor: 'distance' },
    { Header: 'Hang Time', accessor: 'hang_time' },
  ];

  return (
    <Card
      title="Log of all data collected on me for this session."
      classElement="player-log">
      <div className="player-log__header">
        <p className="player-log__total-text">
          Total Session Rows ({totalCount}) - Practice
        </p>

        <div className="player-log__filter">
          <Select
            styles={customStylesSelect}
            options={optionsSelect}
            placeholder="Show"
            onChange={(option) => {
              if (option) {
                setFilters({
                  ...filters,
                  count: option.value,
                });
              }
              return option;
            }}
          />
        </div>
      </div>

      <div className="player-log__table player-log-table">
        <div className="player-log-table__th">
          <div className="table-data__header table-data__header--log">
            Pitcher Name
          </div>
          <div className="table-data__header table-data__header--log">
            Pitcher Handedness
          </div>
          <div className="table-data__header table-data__header--log">
            Batter Name
          </div>
          <div className="table-data__header table-data__header--log">
            Pitch Type
          </div>
          <div className="table-data__header table-data__header--log">
            Pitch Call
          </div>
          <div className="table-data__header table-data__header--log">
            Velocity
          </div>
          <div className="table-data__header table-data__header--log">
            Spin Rate
          </div>
        </div>
        <div>
          {asyncPlayerLog.loading && <LoaderData />}
          {asyncPlayerLog.result &&
            dataTable.map((data, index) => {
              return (
                <div
                  className="player-log-table__element"
                  key={data.batter_name + data.id}>
                  <div
                    className="table-data__row player-log-table__row"
                    data-toggle="collapse"
                    data-target={`.multi-collapse${index}`}
                    aria-controls={`multiCollapseExample${index}`}>
                    <div className="table-data__coll table-data__coll--log">
                      {data.pitcher_name || '-'}
                    </div>
                    <div className="table-data__coll table-data__coll--log">
                      {data.pitcher_handedness || '-'}
                    </div>
                    <div className="table-data__coll table-data__coll--log">
                      {data.batter_name || '-'}
                    </div>
                    <div className="table-data__coll table-data__coll--log">
                      {data.pitch_type || '-'}
                    </div>
                    <div className="table-data__coll table-data__coll--log">
                      {data.pitch_call || '-'}
                    </div>
                    <div className="table-data__coll table-data__coll--log">
                      {data.velocity || '-'}
                    </div>
                    <div className="table-data__coll table-data__coll--log">
                      {data.spin_rate || '-'}
                    </div>
                  </div>
                  <div
                    className={`collapse multi-collapse${index}`}
                    id={`multiCollapseExample${index}`}>
                    <TableCustom columns={columnTable} data={dataTable} />
                  </div>
                </div>
              );
            })}
          {(dataTable.length > 0 || lengthPagination !== 0) &&
            totalCount > filters.count && (
              <ReactPaginate
                previousLabel={
                  <FontAwesomeIcon
                    icon={faAngleDoubleLeft}
                    color="#667784"
                    width="6"
                    height="6"
                  />
                }
                nextLabel={
                  <FontAwesomeIcon
                    icon={faAngleDoubleRight}
                    color="#667784"
                    width="6"
                    height="6"
                  />
                }
                breakLabel={'...'}
                breakClassName={'break-me'}
                pageCount={lengthPagination}
                marginPagesDisplayed={1}
                pageRangeDisplayed={3}
                onPageChange={({ selected }) =>
                  setFilters({
                    ...filters,
                    offset: selected * filters.count,
                  })
                }
                containerClassName="pagination"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                activeClassName={'active'}
              />
            )}
        </div>
      </div>
    </Card>
  );
};

export default PlayerLog;
