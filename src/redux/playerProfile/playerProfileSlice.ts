import { createSlice } from '@reduxjs/toolkit';
import { playerProfileOperations } from './index';
import { PlayerProfileState } from './types';

const initialState: PlayerProfileState = {
  playerProfile: null,
  loadingPlayerProfile: 'idle',
  error: undefined,
};

const playerProfileSlice = createSlice({
  name: 'data',
  initialState,
  reducers: {
    changeStatusFavorite: (state) => {
      if (state.playerProfile) {
        state.playerProfile.favorite = !state.playerProfile.favorite;
      }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      playerProfileOperations.fetchPlayerProfile.pending,
      (state) => {
        state.loadingPlayerProfile = 'pending';
      },
    );
    builder.addCase(
      playerProfileOperations.fetchPlayerProfile.fulfilled,
      (state, action) => {
        state.playerProfile = action.payload.data.profile;
        state.loadingPlayerProfile = 'succeeded';
        state.error = undefined;
      },
    );
    builder.addCase(
      playerProfileOperations.fetchPlayerProfile.rejected,
      (state, action) => {
        state.loadingPlayerProfile = 'failed';
        if (action.payload) {
          state.error = action.payload.message;
        } else {
          state.error = action.error.message;
        }
      },
    );
  },
});

export const { changeStatusFavorite } = playerProfileSlice.actions;
export default playerProfileSlice.reducer;
