import { AxiosResponse } from 'axios';
import {
  QUERY_BATTING_GRAPH,
  QUERY_BATTING_LOG,
  QUERY_BATTING_SUMMARY,
  QUERY_CHANGE_FAVORITE_PROFILE,
  QUERY_EVENT_DETAIL,
  QUERY_EVENT_HITTING_SUMMARY,
  QUERY_EVENT_PITCHING_SUMMARY,
  QUERY_EVENT_ROWS,
  QUERY_EVENT_SUMMARY,
  QUERY_FAVORITE_PROFILES,
  QUERY_PITCHING_GRAPH,
  QUERY_PITCHING_LOG,
  QUERY_PITCHING_SUMMARY,
  QUERY_PROFILE,
  QUERY_PROFILES_IN_EVENT,
  QUERY_PROFILE_EVENTS,
  QUERY_PROFILE_NAMES,
} from 'src/const';
import { ResponsePlayerProfile } from 'src/redux/playerProfile/types';
import {
  BattingGraphResponse,
  BattingLogResponse,
  BattingSummaryResponse,
  EventDetailResponse,
  EventHittingSummaryRowsResponse,
  EventPitchingSummaryResponse,
  EventPlayerSummaryResponse,
  EventRowsResponse,
  PitcherGraphResponse,
  PitchingSummaryResponse,
  PithingLogResponse,
  ProfileFavoriteResponse,
  ProfileNamesResponse,
  ProfilesInEventResponse,
  RecentEventTypeResponse,
  UpdateFavoriteProfileResponse,
} from 'src/types';
import { fetchApi } from './fetchApi';

class FetchData {
  async fetchBattingSummary(
    id: string,
  ): Promise<AxiosResponse<BattingSummaryResponse>> {
    const data = {
      query: QUERY_BATTING_SUMMARY,
      variables: {
        id,
      },
    };
    const response = await fetchApi<BattingSummaryResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchPitchingSummary(
    id: string,
  ): Promise<AxiosResponse<PitchingSummaryResponse>> {
    const data = {
      query: QUERY_PITCHING_SUMMARY,
      variables: {
        id,
      },
    };
    const response = await fetchApi<PitchingSummaryResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }
  async fetchEvents(
    id: string,
    filters?: { date?: string; event_type?: string },
  ): Promise<AxiosResponse<RecentEventTypeResponse>> {
    const data = {
      query: QUERY_PROFILE_EVENTS,
      variables: {
        input: {
          profile_id: id,
          offset: 0,
          count: 1000,
          ...filters,
        },
      },
    };
    const response = await fetchApi<RecentEventTypeResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }
  async fetchBattingGraph(
    id: string,
    filters?: { pitch_type: string },
  ): Promise<AxiosResponse<BattingGraphResponse>> {
    const data = {
      query: QUERY_BATTING_GRAPH,
      variables: {
        input: {
          profile_id: id,
          ...filters,
        },
      },
    };
    const response = await fetchApi<BattingGraphResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchPithingGraph(
    id: string,
    filters?: { pitch_type: string },
  ): Promise<AxiosResponse<PitcherGraphResponse>> {
    const data = {
      query: QUERY_PITCHING_GRAPH,
      variables: {
        input: {
          profile_id: id,
          ...filters,
        },
      },
    };
    const response = await fetchApi<PitcherGraphResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchBattingLog(
    id: string,
    filters: { offset: number; count: number; pitcher_name?: string },
  ): Promise<AxiosResponse<BattingLogResponse>> {
    const data = {
      query: QUERY_BATTING_LOG,
      variables: {
        input: {
          profile_id: id,
          ...filters,
        },
      },
    };
    const response = await fetchApi<BattingLogResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchPithingLog(
    id: string,
    filters: { offset: number; count: number; batter_name?: string },
  ): Promise<AxiosResponse<PithingLogResponse>> {
    const data = {
      query: QUERY_PITCHING_LOG,
      variables: {
        input: {
          profile_id: id,
          ...filters,
        },
      },
    };
    const response = await fetchApi<PithingLogResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchProfileNames(filters: {
    player_name?: string;
    position: string;
  }): Promise<AxiosResponse<ProfileNamesResponse>> {
    const data = {
      query: QUERY_PROFILE_NAMES,
      variables: {
        input: {
          ...filters,
        },
      },
    };
    const response = await fetchApi<ProfileNamesResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchProfile(
    id: string,
  ): Promise<AxiosResponse<ResponsePlayerProfile>> {
    const data = {
      query: QUERY_PROFILE,
      variables: {
        id,
      },
    };
    const response = await fetchApi<ResponsePlayerProfile>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchFavoriteProfiles(): Promise<
    AxiosResponse<ProfileFavoriteResponse>
  > {
    const data = {
      query: QUERY_FAVORITE_PROFILES,
      variables: {
        input: { profiles_count: 50, offset: 0 },
      },
    };
    const response = await fetchApi<ProfileFavoriteResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchUpdateFavorite(
    id: string,
    favorite: boolean,
  ): Promise<AxiosResponse<UpdateFavoriteProfileResponse>> {
    const data = {
      query: QUERY_CHANGE_FAVORITE_PROFILE,
      variables: {
        form: { profile_id: id, favorite },
      },
    };
    const response = await fetchApi<UpdateFavoriteProfileResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchProfilesInEvent(
    idEvent: string,
    profiles_count?: 100 | number,
  ): Promise<AxiosResponse<ProfilesInEventResponse>> {
    const data = {
      query: QUERY_PROFILES_IN_EVENT,
      variables: {
        input: { event: idEvent, profiles_count },
      },
    };
    const response = await fetchApi<ProfilesInEventResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchEventDetail(
    idEvent: string,
    idProfile: string,
  ): Promise<AxiosResponse<EventDetailResponse>> {
    const data = {
      query: QUERY_EVENT_DETAIL,
      variables: {
        input: { event_id: idEvent, profile_id: idProfile },
      },
    };
    const response = await fetchApi<EventDetailResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchEventPlayerSummary(
    idEvent: string,
    idProfile: string,
  ): Promise<AxiosResponse<EventPlayerSummaryResponse>> {
    const data = {
      query: QUERY_EVENT_SUMMARY,
      variables: {
        input: { event_id: idEvent, profile_id: idProfile },
      },
    };
    const response = await fetchApi<EventPlayerSummaryResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchEventPlayerLog(
    idEvent: string,
    idProfile: string,
    filters: { count: number; offset: number },
  ): Promise<AxiosResponse<EventRowsResponse>> {
    const data = {
      query: QUERY_EVENT_ROWS,
      variables: {
        input: { event_id: idEvent, profile_id: idProfile, ...filters },
      },
    };
    const response = await fetchApi<EventRowsResponse>('/api/v1/graphql', data);

    return response;
  }

  async fetchEventHittingSummary(
    idEvent: string,
    idProfile: string,
  ): Promise<AxiosResponse<EventHittingSummaryRowsResponse>> {
    const data = {
      query: QUERY_EVENT_HITTING_SUMMARY,
      variables: {
        input: { event_id: idEvent, profile_id: idProfile },
      },
    };
    const response = await fetchApi<EventHittingSummaryRowsResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }

  async fetchEventPitchingSummary(
    idEvent: string,
    idProfile: string,
  ): Promise<AxiosResponse<EventPitchingSummaryResponse>> {
    const data = {
      query: QUERY_EVENT_PITCHING_SUMMARY,
      variables: {
        input: { event_id: idEvent, profile_id: idProfile },
      },
    };
    const response = await fetchApi<EventPitchingSummaryResponse>(
      '/api/v1/graphql',
      data,
    );

    return response;
  }
}

export default new FetchData();
