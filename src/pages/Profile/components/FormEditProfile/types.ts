import { Facility, School, Teams } from 'src/types';

export interface TeamsResponse {
  data: {
    teams: {
      teams: Teams;
    };
  };
}

export interface SchoolResponse {
  data: {
    schools: {
      schools: School;
    };
  };
}

export interface FacilitiesResponse {
  data: {
    facilities: {
      facilities: Facility;
    };
  };
}
