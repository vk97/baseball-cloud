import React from 'react';
import MainContainer from '../../components/MainContainer';
import FormForgotPassword from '../../components/FormForgotPassword';
import { MainLayoutModification } from 'src/types';

const ForgotPassword = (): JSX.Element => {
  return (
    <MainContainer styleModification={MainLayoutModification.AUTHORIZATION}>
      <FormForgotPassword />
    </MainContainer>
  );
};

export default ForgotPassword;
