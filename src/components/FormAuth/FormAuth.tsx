import { FORM_ERROR } from 'final-form';
import React from 'react';
import { Form, Field } from 'react-final-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { AppDispatch } from 'src/redux/store';
import { userSelectors, userOperations } from 'src/redux/user';
import { UserSignInRequest } from 'src/types';
import { required } from 'src/utils/validate';
import FieldButton from '../FieldButton';
import FormContainer from '../FormContainer';
import IconPassword from '../Icons/IconPassword';
import IconUser from '../Icons/IconUser';
import Input from '../Input';
import './styles.scss';

const FormAuth = (): JSX.Element => {
  const dispatch: AppDispatch = useDispatch();
  const loading = useSelector(userSelectors.selectStatusLoading);

  const handleSignIn = async (userData: UserSignInRequest) => {
    const resultAction = await dispatch(userOperations.fetchSignIn(userData))
    if (!userOperations.fetchSignIn.fulfilled.match(resultAction)) {
      if (resultAction.error) {
        return {[FORM_ERROR]: resultAction.error.message};
      }
    }
  }

  return (
    <FormContainer>
      <div className="form-container__header">
        <p className="form-container__title">Welcome to BaseballCloud!</p>
        <p className="form-container__subtitle">Sign into your account here:</p>
      </div>

      <Form
        onSubmit={handleSignIn}
        render={({ handleSubmit, submitError }) => (
          <form className="form-auth" onSubmit={handleSubmit}>
            <Field
              name="email"
              placeholder="Email"
              type="email"
              validate={required}
              render={({ input, meta, placeholder }) => (
                <p className="form-auth__input-group">
                  <Input placeholder={placeholder} {...input}>
                    <IconUser />
                  </Input>
                  {meta.touched && meta.error && (
                    <span className="form-auth__text-error">{meta.error}</span>
                  )}
                </p>
              )}
            />

            <Field
              name="password"
              placeholder="Password"
              type="password"
              validate={required}
              render={({ input, meta, placeholder }) => (
                <p className="form-auth__input-group">
                  <Input placeholder={placeholder} {...input}>
                    <IconPassword />
                  </Input>
                  {meta.touched && meta.error && (
                    <span className="form-auth__text-error">{meta.error}</span>
                  )}
                </p>
              )}
            />
            <span className="form-auth__text-error form-auth__text-error--request">
              {submitError}
            </span>
            <FieldButton title="Sign In" disabled={loading === 'pending'} />
          </form>
        )}
      />
      <div className="form-container__forgot">
        <Link to="/forgotpassword" className="form-container__link-forgot">
          Forgotten password?
        </Link>
      </div>

      <div className="form-container__sign-up sign-up">
        <span className="sing-up__text">Don’t have an account? </span>
        <Link to="/registration" className="sign-up__link">
          Sign Up
        </Link>
      </div>
    </FormContainer>
  );
};

export default FormAuth;
